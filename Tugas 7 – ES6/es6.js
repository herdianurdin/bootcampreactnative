// Soal 1 (Mengubah fungsi menjadi fungsi arrow)
console.log('Soal 1 (Mengubah fungsi menjadi fungsi arrow)')

// Original Function
// const golden = function golden() {
//     console.log('this is golden!!')
// }

// Arrow Function
const golden = () => {
    console.log('this is golden!!')
}

golden()

console.log()

// Soal 2 (Menyederhanakan object literal)
console.log('Soal 2 (Menyederhanakan object literal)')

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        // Can't use arrow will impact to return
        fullName: function () {
            console.log(`${this.firstName} ${this.lastName}`)
            return
        }
    }
}
const person = newFunction('William', 'Imoh')
console.log(person)
person.fullName()

console.log()

// Soal 3 (Destructuring)
console.log('Soal 3 (Destructuring)')

const newObject = {
    firstName: 'Harry',
    lastName: 'Potter Holt',
    destination: 'Hogwarts React Conf',
    occupation: 'Deve-wizard Avocado',
    spell: 'Vimulus Renderus!!!'
}

const { firstName, lastName, destination, occupation } = newObject

console.log(firstName, lastName, destination, occupation)

console.log()

// Soal 4 (Array Spreading)
console.log('Soal 4 (Array Spreading)')

const west = ['Will', 'Chris', 'Sam', 'Holly']
const east = ['Gill', 'Brian', 'Noel', 'Maggie']

// Original Code
// const combined = west.concat(east)

// Spreading Code
const combined = [...west, ...east]

console.log(combined)

console.log()

// Soal 5 (Templating Literals)
console.log('Soal 5 (Templating Literals)')

const planet = 'earth'
const view = 'glass'
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before) 
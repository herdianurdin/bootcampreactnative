// Soal 1 (Range)
console.log("Soal 1 (Range)");

function range(startNum, finishNum) {
    const data = [];

    if (startNum == undefined || finishNum == undefined) {
        return -1;
    } else if (startNum <= finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            data.push(i);
        }
    } else if (startNum >= finishNum) {
        for (var i = startNum; i >= finishNum; i--) {
            data.push(i);
        }
    }

    return data;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log();

// Soal 2 (Range with Step)
console.log("Soal 2 (Range with Step)");

function rangeWithStep(startNum, finishNum, step) {
    const data = [];

    if (startNum == undefined || finishNum == undefined || step == undefined || step <= 0) {
        return -1;
    } else if (startNum <= finishNum) {
        for (var i = startNum; i <= finishNum; i += step) {
            data.push(i);
        }
    } else if (startNum >= finishNum) {
        for (var i = startNum; i >= finishNum; i -= step) {
            data.push(i);
        }
    }

    return data;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log();

// Soal 3 (Sum of Range)
console.log("Soal 3 (Sum of Range)");

/*
    Saya gunakan default parameter pada fungsi agar mempersingkat
    penulisan kode, tanpa harus menggunakan if statment
*/

// Solution 1 => using forEach
// function sum(start = 0, stop = 0, step = 1) {
//     var result = 0

//     rangeWithStep(start, stop, step).forEach(function (n) {
//         result += n
//     })

//     return result
// }

// Solution 2 => using reduce
function sum(start = 0, stop = 0, step = 1) {
    return rangeWithStep(start, stop, step).reduce(
        function (total, number) {
            return total + number;
        }, 0 // Initial Value diperlukan jika array nya tidak memiliki element
    );
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log();

// Soal 4 (Array Multidimensi)
console.log("Soal 4 (Array Multidimensi)");

function dataHandling(data) {
    var result = "";

    data.forEach(function (i) {
        // Enak dilihat
        // result += `Nomor ID:\t${i[0]}\nNama Lengkap:\t${i[1]}\nTTL:\t\t${i[2]} ${i[3]}\nHobi:\t\t${i[4]}\n\n`;

        result += `Nomor ID:  ${i[0]}\nNama Lengkap:  ${i[1]}\nTTL:  ${i[2]} ${i[3]}\nHobi:  ${i[4]}\n\n`;
    });

    return result;
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
console.log(dataHandling(input));

// Soal 5 (Balik Kata)
console.log("Soal 5 (Balik Kata)");

// Solution 1 => Traditional
function balikKata(word) {
    var result = "";

    for (var i = word.length - 1; i >= 0; i--) {
        result += word[i];
    }

    return result;
}

// Solution 2 => Lazy Solution
// function balikKata(word) {
//     return word.split("").reverse().join("");
// }

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log();

// Soal 6 (Metode Array)
console.log("Soal 6 (Metode Array)");

// Using Array
// function monthName(monthNumber) {
//     const monthNames = [
//         "Januari", "Februari", "Maret", "April",
//         "Mei", "Juni", "Juli", "Agustus",
//         "September", "Oktober", "November", "Desember",
//     ];

//     return monthNames[monthNumber - 1] || null;
// }

// Using Switch Case
function monthName(monthNumber) {
    var month = ""

    switch (monthNumber) {
        case 1:
            month = "Januari";
            break;
        case 2:
            month = "Februari";
            break;
        case 3:
            month = "Maret";
            break;
        case 4:
            month = "April";
            break;
        case 5:
            month = "Mei";
            break;
        case 6:
            month = "Juni";
            break;
        case 7:
            month = "Juli";
            break;
        case 8:
            month = "Agustus";
            break;
        case 9:
            month = "September";
            break;
        case 10:
            month = "Oktober";
            break;
        case 11:
            month = "November";
            break;
        case 12:
            month = "Desember";
            break;
    }

    return month;
}

function dataHandling2(data = []) {
    // For First Output
    data[1] = `${data[1].trim()} Elsharawy`;
    data[2] = `Provinsi ${data[2].trim()}`;
    data.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(data);

    const date = data[3].split("/");

    // For Second Output
    console.log(monthName(parseInt(date[1])));

    // For Third Output
    // console.log([date[2], date[0], date[1]]);
    const dateSorted = data[3].split("/").sort(function (x, y) {
        return y - x;
    });
    console.log(dateSorted);

    // For Fourth Output
    console.log(date.join("-"));

    // For Fifth Output
    console.log(data[1].slice(0, 15));
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

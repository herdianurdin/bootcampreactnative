// Soal 1 (Array to Object)
console.log("Soal 1 (Array to Object)");

const thisYear = new Date().getFullYear(); // 2021

function arrayToObject(arr) {
    const peoples = {};
    const keys = ["firstName", "lastName", "gender", "age"];

    for (var i = 0; i < arr.length; i++) {
        const people = {};

        for (var k = 0; k < keys.length - 1; k++) {
            people[keys[k]] = arr[i][k];
        }

        people["age"] = arr[i][3] == undefined || arr[i][3] > thisYear ? "Invalid Birth Year" : thisYear - arr[i][3];
        peoples[i + 1 + ". " + people["firstName"] + " " + people["lastName"]] = people;
    }

    return peoples;
}

var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"],
];
var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023],
];

console.log(arrayToObject(people));
console.log(arrayToObject(people2));
console.log(arrayToObject([]))

console.log();

// Soal 2 (Shopping Time)
console.log("Soal 2 (Shopping Time)");

const stock = [
    { barang: "Sepatu Stacattu", harga: 1500000 },
    { barang: "Baju Zoro", harga: 500000 },
    { barang: "Baju H&N", harga: 250000 },
    { barang: "Sweater Uniklooh", harga: 175000 },
    { barang: "Casing Handphone", harga: 50000 },
];

function shoppingTime(memberId, money) {
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup";
    }

    const receipt = {};

    receipt["memberId"] = memberId;
    receipt["money"] = money;
    receipt["listPurchased"] = [];

    stock.forEach(function (s) {
        if (money >= s.harga) {
            receipt.listPurchased.push(s.barang);
            money -= s.harga;
        }
    });

    receipt["changeMoney"] = money;

    return receipt;
}

console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

console.log();

// Soal 3 (Naik Angkot)
console.log("Soal 3 (Naik Angkot)");

function naikAngkot(passengers = []) {
    const rute = ["A", "B", "C", "D", "E", "F"];
    const price = 2000;
    const result = [];

    passengers.forEach(function (passenger) {
        result.push({
            penumpang: passenger[0],
            naikDari: passenger[1],
            tujuan: passenger[2],
            bayar: (rute.indexOf(passenger[2]) - rute.indexOf(passenger[1])) * price
        });
    });

    return result;
}

console.log(naikAngkot(
    [
        ["Dimitri", "B", "F"],
        ["Icha", "A", "B"]
    ]
));
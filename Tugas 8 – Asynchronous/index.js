const readBooks = require('./callback.js')

const books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

let timeRemaining = 10000
let i = 0

/* 
    Fungsi untuk callback bisa digunakan secara
    inline, tetapi agar penulisan mudah dibaca
    kode ditulis secara terpisah.
*/
const reading = (time) => {
    if (books.length - 1 > i++ && timeRemaining != time) {
        timeRemaining = time

        readBooks(timeRemaining, books[i], reading)
    }
}

readBooks(timeRemaining, books[i], reading)
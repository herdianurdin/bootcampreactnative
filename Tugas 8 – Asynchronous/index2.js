const readBooksPromise = require('./promise.js')

const books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

let timeRemaining = 10000
let i = 0

/* 
    Fungsi yang digunakan untuk Promise, bisa digunakan
    secara inline, tetapi agar penulisan mudah dibaca
    kode ditulis secara terpisah.
*/
const reading = (time) => {
    if (books.length - 1 > i++) {
        timeRemaining = time
        readBooksPromise(timeRemaining, books[i])
            .then(reading)
            .catch(() => { })
    }
}

readBooksPromise(timeRemaining, books[i])
    .then(reading)
    .catch(() => { })
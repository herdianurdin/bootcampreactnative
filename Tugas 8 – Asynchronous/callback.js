const readBooks = (time, book, callback) => {
    if (time > 0) {
        console.log(`saya membaca ${book.name}`)
    }

    setTimeout(() => {
        if (time >= book.timeSpent) {
            const sisaWaktu = time - book.timeSpent

            console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            callback(sisaWaktu)
        } else {
            console.log('waktu saya habis')
            callback(time)
        }
    }, (time >= book.timeSpent ? book.timeSpent : time))
}

module.exports = readBooks
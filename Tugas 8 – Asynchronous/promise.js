const readBooksPromise = (time, book) => {
    if (time > 0) {
        console.log(`saya mulai membaca ${book.name}`)
    }

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let sisaWaktu = time - book.timeSpent

            if (sisaWaktu >= 0) {
                console.log(`saya sudah selesai membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
                resolve(sisaWaktu)
            } else {
                console.log(`saya sudah tidak punya waktu untuk baca ${book.name}`)
                reject(sisaWaktu)
            }
        }, (time >= book.timeSpent ? book.timeSpent : time))
    })
}

module.exports = readBooksPromise
// Soal 1 (Animal Class) Release 0
console.log('Soal 1 (Animal Class)')

class Animal {
    constructor(name) {
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }

    get name() {
        return this._name
    }

    get legs() {
        return this._legs
    }

    set legs(legs) {
        this._legs = legs
    }

    get cold_blooded() {
        return this._cold_blooded
    }

    set cold_blooded(cold_blooded) {
        this._cold_blooded = cold_blooded
    }
}

const sheep = new Animal('shaun')

console.log('=========== Release 0 ===========')
console.log(sheep.name) // 'shaun'
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log()

// Soal 1 (Inheritance) Release 1
class Ape extends Animal {
    constructor(name) {
        super(name)
        super.legs = 2
    }

    yell() {
        console.log('Auooo')

        // return 'Auooo'
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
        super.cold_blooded = true
    }

    jump() {
        console.log('hop hop')

        // return 'hop hop'
    }
}

console.log('=========== Release 1 ===========')
const sungokong = new Ape('kera sakti')
console.log(sungokong.name) // 'kera sakti'
console.log(sungokong.legs) // 2
console.log(sungokong.cold_blooded) // false
sungokong.yell() // 'Auooo'

console.log()

const kodok = new Frog('buduk')
console.log(kodok.name) // 'buduk'
console.log(kodok.legs) // 4
console.log(kodok.cold_blooded) // true
kodok.jump() // 'hop hop'

console.log()

// Soal 2 (Function to Class)
console.log('Soal 2 (Function to Class)')

class Clock {
    constructor({ template }) {
        this.template = template
        this.timer = null
    }

    render() {
        this.date = new Date()

        let hours = this.date.getHours()
        hours = hours < 10 ? `0${hours}` : hours

        let mins = this.date.getMinutes()
        mins = mins < 10 ? `0${mins}` : mins

        let secs = this.date.getSeconds()
        secs = secs < 10 ? `0${secs}` : secs

        const output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs)

        console.log(output)
    }

    stop() {
        clearInterval(this.timer)
    }

    start() {
        this.render()
        this.timer = setInterval(() => {
            this.render()
        }, 1000)
    }
}

const clock = new Clock({ template: 'h:m:s' })
clock.start()
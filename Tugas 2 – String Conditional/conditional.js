// Soal 1 (IF-ELSE)
console.log("Soal 1 (IF-ELSE)");

var nama = "John";
var peran = "";

// Saya belum pernah mendengar orang dengan nama karakter spasi ' '
// bisa juga nama.length < 1 dan peran.length < 1, tetapi karena permasalahan diatas
if (nama == "" || nama == " ") {
    console.log("Nama harus diisi!");
} else if (peran == "" || peran == " ") {
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
} else {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);

    const greeting = `Halo ${peran} ${nama},`;

    if (peran == "Penyihir") {
        console.log(greeting, "kamu dapat melihat siapa saja yang menjadi werewolf!");
    } else if (peran == "Guard") {
        console.log(greeting, "kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if (peran == "Werewolf") {
        console.log(greeting, "Kamu akan memakan mangsa setiap malam!");
    }
}

console.log();

// Soal 2 (Switch Case)
console.log("Soal 2 (Switch Case)");
var hari = 21;
var bulan = 1;
var tahun = 1945;

switch (bulan) {
    case 1:
        bulan = "Januari";
        break;
    case 2:
        bulan = "Februari";
        break;
    case 3:
        bulan = "Maret";
        break;
    case 4:
        bulan = "April";
        break;
    case 5:
        bulan = "Mei";
        break;
    case 6:
        bulan = "Juni";
        break;
    case 7:
        bulan = "Juli";
        break;
    case 8:
        bulan = "Agustus";
        break;
    case 9:
        bulan = "September";
        break;
    case 10:
        bulan = "Oktober";
        break;
    case 11:
        bulan = "November";
        break;
    case 12:
        bulan = "Desember";
        break;
    default:
        break;
}

var tanggal = hari;

console.log(`${tanggal} ${bulan} ${tahun}`);

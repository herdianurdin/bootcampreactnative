// Soal 1 (Membuat Kalimat)
console.log("Soal 1 (Membuat Kalimat)");

var word = "JavaScript";
var second = "is";
var third = "awesome";
var fourth = "and";
var fifth = "I";
var sixth = "love";
var seventh = "it!";

// Solution 1 => Tradisional
// var outString = word + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh

// Solution 2 => Dengan tanda `
// var outString = `${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`

// Solution 3 => Penulisan orang malas neken tombol "shift"
// var outString = [word, second, third, fourth, fifth, sixth, seventh].join(" ");

// console.log(outString, "\n");

// Solution 4 => Pemalas
console.log(word, second, third, fourth, fifth, sixth, seventh);

console.log();

// Soal 2 (Mengurai Kalimat) => Akses char string
console.log("Soal 2 (Mengurai Kalimat) => Akses char string");

var sentence = "I am going to be React Native Developer";

var firstWord = sentence[0];
var secondWord = sentence[2] + sentence[3];
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12];
var fifthWord = sentence[14] + sentence[15];
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log("First Word: " + firstWord);
console.log("Second Word: " + secondWord);
console.log("Third Word: " + thirdWord);
console.log("Fourth Word: " + fourthWord);
console.log("Fifth Word: " + fifthWord);
console.log("Sixth Word: " + sixthWord);
console.log("Seventh Word: " + seventhWord);
console.log("Eighth Word: " + eighthWord);

console.log();

// Soal 3 (Mengurai Kalimat) => substring
console.log("Soal 3 (Mengurai Kalimat) => substring");

var sentence2 = "wow JavaScript is so cool";

var firstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);

console.log("First Word: " + firstWord2);
console.log("Second Word: " + secondWord2);
console.log("Third Word: " + thirdWord2);
console.log("Fourth Word: " + fourthWord2);
console.log("Fifth Word: " + fifthWord2);

console.log();

// Soal 4 Mengurai Kalimat dan Menentukan Panjang String
console.log("Soal 4 Mengurai Kalimat dan Menentukan Panjang String");

var sentence3 = "wow JavaScript is so cool";

// Solution 1 => substring
// var firstWord3 = sentence3.substring(0, 3);
// var secondWord3 = sentence3.substring(4, 14);
// var thirdWord3 = sentence3.substring(15, 17);
// var fourthWord3 = sentence3.substring(18, 20);
// var fifthWord3 = sentence3.substring(21, 25);

// Solution 2 => substr
// var firstWord3 = sentence3.substr(0, 3);
// var secondWord3 = sentence3.substr(4, 10);
// var thirdWord3 = sentence3.substr(15, 2);
// var fourthWord3 = sentence3.substr(18, 2);
// var fifthWord3 = sentence3.substr(21, 4);

// Solution 3 => split
sentence3 = sentence3.split(" ");

var firstWord3 = sentence3[0];
var secondWord3 = sentence3[1];
var thirdWord3 = sentence3[2];
var fourthWord3 = sentence3[3];
var fifthWord3 = sentence3[4];

console.log("First Word: " + firstWord3 + ", with length: " + firstWord3.length);
console.log("Second Word: " + secondWord3 + ", with length: " + secondWord3.length);
console.log("Third Word: " + thirdWord3 + ", with length: " + thirdWord3.length);
console.log("Fourth Word: " + fourthWord3 + ", with length: " + fourthWord3.length);
console.log("Fifth Word: " + fifthWord3 + ", with length: " + fifthWord3.length);

import * as React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

import Home from '../Pages/Home'
import AboutScreen from '../Pages/AboutScreen'
import AddScreen from '../Pages/AddScreen'
import Login from '../Pages/Login'
import ProjectScreen from '../Pages/ProjectScreen'
import Setting from '../Pages/Setting'
import SkillProject from '../Pages/SkillProject'

const Tab = createBottomTabNavigator()
const Drawer = createDrawerNavigator()
const Stack = createNativeStackNavigator()

const MainApp = () => (
    <Tab.Navigator>
        <Tab.Screen
            name="AboutScreen"
            component={AboutScreen}
            options={{
                headerShown: false,
            }}
        />
        <Tab.Screen
            name="AddScreen"
            component={AddScreen}
            options={{
                headerShown: false,
            }}
        />
        <Tab.Screen
            name="SkillProject"
            component={SkillProject}
            options={{
                headerShown: false,
            }}
        />
    </Tab.Navigator>
)

const MyDrawer = () => (
    <Drawer.Navigator>
        <Drawer.Screen
            name="App"
            component={MainApp}
            options={{
                headerShown: false,
            }}
        />
        <Drawer.Screen
            name="AboutScreen"
            component={AboutScreen}
            options={{
                headerShown: false,
            }}
        />
    </Drawer.Navigator>
)

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Home" component={Home} />
                <Stack.Screen name="MainApp" component={MainApp} />
                <Stack.Screen name="MyDrawer" component={MyDrawer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({})

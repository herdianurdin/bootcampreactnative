import * as React from 'react'
import * as Font from 'expo-font'
import { StatusBar } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Login from './screens/Login'
import Register from './screens/Register'
import Skills from './screens/Skills'
import About from './screens/About'

const Stack = createNativeStackNavigator()
const Fonts = {
    ubuntuBold: require('./assets/fonts/Ubuntu-Bold.ttf'),
    ubuntuMedium: require('./assets/fonts/Ubuntu-Medium.ttf'),
    ubuntuRegular: require('./assets/fonts/Ubuntu-Regular.ttf'),
}

export default class FirebaseApp extends React.Component {
    state = {
        fontsLoaded: false,
    }

    async _loadFontsAsync() {
        await Font.loadAsync(Fonts)
        this.setState({ fontsLoaded: true })
    }

    componentDidMount() {
        this._loadFontsAsync()
        StatusBar.setHidden(true, 'none')
    }

    render() {
        if (!this.state.fontsLoaded) {
            return null
        }

        return (
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
                    <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
                    <Stack.Screen name="Skills" component={Skills} options={{ headerShown: false }} />
                    <Stack.Screen name="About" component={About} options={{ headerShown: false }} />
                </Stack.Navigator>
            </NavigationContainer>
        )
    }
}

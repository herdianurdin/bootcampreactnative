const colors = {
    backgroundWhite: '#FFFFFF',
    backgroundPrimary: '#29B6F6',
    backgroundLight: '#F5F5F5',
    textDark: '#2F2F2F',
    textMuted: '#90A7B2',
    textPrimary: '#29B6F6',
    textWhite: '#FFFFFF',
    javascript: '#F7DF1E',
    python: '#2F9BFE',
    react: '#5CCFEE',
    tensorflow: '#F28C00'
}

export default colors
import * as React from 'react'
import { View, Text, Image, TextInput, KeyboardAvoidingView, TouchableOpacity } from 'react-native'
import styles from './styles'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import colors from '../../assets/colors'

const Login = ({ navigation }) => {
    const [ show, setShow ] = React.useState(false)
    const [ visible, setVisible ] = React.useState(true)

    return (
        <View style={styles.container}>
            <View style={styles.headerApp}>
                <Image source={require('../../assets/images/Port.png')} style={styles.headerLogo} />
                <View style={styles.headerText}>
                    <Text style={styles.title}>Welcome</Text>
                    <Text style={styles.title}>back!</Text>
                </View>
            </View>
            <View style={styles.formInput}>
                <KeyboardAvoidingView>
                    <TextInput placeholder={'Email or Username'} style={styles.inputEmail} />
                </KeyboardAvoidingView>
                <KeyboardAvoidingView>
                    <TextInput secureTextEntry={visible} placeholder={'Password'} style={styles.inputPassword } />
                    <TouchableOpacity style={styles.buttonEye} onPress={
                        () => {
                            setVisible(!visible)
                            setShow(!show)
                        }
                    }>
                        <MaterialCommunityIcons name={!show ? 'eye' : 'eye-off'} size={24} color={colors.textMuted} />
                    </TouchableOpacity>
                </KeyboardAvoidingView>
            </View>
            <TouchableOpacity onPress={() => {
                navigation.navigate('Skills')
            }} style={styles.buttonLogin}>
                    <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <View>
                    <Text style={styles.forgotPassword}>Forgot Password?</Text>
                </View>
            </TouchableOpacity>
            <View style={styles.footerApp}>
                <Text style={styles.footerText}>Don't have any account? </Text>
                <TouchableOpacity onPress={() => {
                    navigation.navigate('Register')
                }}>
                    <Text style={styles.footerTextLink}>Register</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Login
import * as React from 'react'
import { View, Text, Image, TextInput, KeyboardAvoidingView, TouchableOpacity } from 'react-native'
import styles from './styles'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import colors from '../../assets/colors'

const Register = ({ navigation }) => {
    const [showPassword, setShowPassword] = React.useState(false)
    const [showConfirmPassword, setShowConfirmPassword] = React.useState(false)
    const [visiblePassword, setVisiblePassword] = React.useState(true)
    const [visibleConfirmPassword, setVisibleConfirmPassword] = React.useState(true)

    return (
        <View style={styles.container}>
            <View style={styles.headerApp}>
                <Image source={require('../../assets/images/Port.png')} style={styles.headerLogo} />
                <View style={styles.headerText}>
                    <Text style={styles.title}>Create new</Text>
                    <Text style={styles.title}>account!</Text>
                </View>
            </View>
            <View style={styles.formInput}>
                <KeyboardAvoidingView>
                    <TextInput placeholder={'Username'} style={styles.inputText} />
                </KeyboardAvoidingView>
                <KeyboardAvoidingView>
                    <TextInput placeholder={'Email'} style={styles.inputText} />
                </KeyboardAvoidingView>
                <KeyboardAvoidingView>
                    <TextInput
                        secureTextEntry={visiblePassword}
                        placeholder={'Password'}
                        style={styles.inputPassword}
                    />
                    <TouchableOpacity
                        style={styles.buttonEye}
                        onPress={() => {
                            setVisiblePassword(!visiblePassword)
                            setShowPassword(!showPassword)
                        }}
                    >
                        <MaterialCommunityIcons
                            name={!showPassword ? 'eye' : 'eye-off'}
                            size={24}
                            color={colors.textMuted}
                        />
                    </TouchableOpacity>
                </KeyboardAvoidingView>
                <KeyboardAvoidingView>
                    <TextInput
                        secureTextEntry={visibleConfirmPassword}
                        placeholder={'Confirm Password'}
                        style={styles.inputPassword}
                    />
                    <TouchableOpacity
                        style={styles.buttonEye}
                        onPress={() => {
                            setVisibleConfirmPassword(!visibleConfirmPassword)
                            setShowConfirmPassword(!showConfirmPassword)
                        }}
                    >
                        <MaterialCommunityIcons
                            name={!showConfirmPassword ? 'eye' : 'eye-off'}
                            size={24}
                            color={colors.textMuted}
                        />
                    </TouchableOpacity>
                </KeyboardAvoidingView>
            </View>
            <TouchableOpacity
                onPress={() => {
                    navigation.navigate('Login')
                }}
                style={styles.buttonRegister}
            >
                <Text style={styles.buttonText}>Register</Text>
            </TouchableOpacity>
            <View style={styles.footerApp}>
                <Text style={styles.footerText}>Have an account? </Text>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('Login')
                    }}
                >
                    <Text style={styles.footerTextLink}>Login</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Register

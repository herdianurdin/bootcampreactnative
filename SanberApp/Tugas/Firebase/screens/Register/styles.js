import { StyleSheet } from 'react-native'
import colors from '../../assets/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.backgroundWhite,
        alignItems: 'center',
    },
    headerApp: {
        width: '100%',
        marginTop: 80,
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 48,
    },
    headerLogo: {
        width: 64,
        height: 64,
    },
    headerText: {
        marginLeft: 16,
    },
    title: {
        fontFamily: 'ubuntuMedium',
        fontSize: 32,
        lineHeight: 32,
        color: colors.textDark,
    },
    formInput: {
        width: '100%',
        marginBottom: 16,
        paddingHorizontal: 36,
    },
    inputText: {
        fontFamily: 'ubuntuMedium',
        fontSize: 16,
        width: '100%',
        backgroundColor: colors.backgroundLight,
        color: colors.textMuted,
        paddingVertical: 8,
        paddingHorizontal: 28,
        borderRadius: 18,
        marginBottom: 20,
    },
    inputPassword: {
        fontFamily: 'ubuntuMedium',
        fontSize: 16,
        width: '100%',
        backgroundColor: colors.backgroundLight,
        color: colors.textMuted,
        paddingVertical: 8,
        paddingLeft: 28,
        paddingRight: 64,
        borderRadius: 18,
        marginBottom: 20,
    },
    buttonEye: {
        position: 'absolute',
        right: 28,
        top: 10,
    },
    buttonRegister: {
        alignItems: 'center',
        marginBottom: 20,
        justifyContent: 'center',
        paddingHorizontal: 36,
        width: '100%'
    },
    buttonText: {
        backgroundColor: colors.backgroundPrimary,
        width: '100%',
        height: 54,
        fontSize: 18,
        fontFamily: 'ubuntuBold',
        color: colors.textWhite,
        textAlign: 'center',
        borderRadius: 18,
        textAlignVertical: 'center'
    },
    footerApp: {
        position: 'absolute',
        bottom: 41,
        flexDirection: 'row',
    },
    footerText: {
        color: colors.textDark,
        fontSize: 14,
        fontFamily: 'ubuntuRegular',
    },
    footerTextLink: {
        fontFamily: 'ubuntuBold',
        color: colors.textPrimary,
    },
})

export default styles

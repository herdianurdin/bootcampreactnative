import { StyleSheet } from 'react-native'
import colors from '../../assets/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.backgroundWhite,
    },
    mainWrapper: {
        width: '100%',
    },
    wrapper: {
        paddingHorizontal: 28,
    },
    headerApp: {
        backgroundColor: colors.backgroundPrimary,
        height: 350,
        alignItems: 'center',
        paddingHorizontal: 28
    },
    headerTitle: {
        justifyContent: 'center',
        width: '100%',
        marginHorizontal: 32,
        alignItems: 'center',
        marginTop: 32,
        marginBottom: 12,
    },
    buttonBack: {
        position: 'absolute',
        left: 0,
    },
    headerTitleText: {
        fontSize: 32,
        fontFamily: 'ubuntuBold',
        color: colors.textWhite,
    },
    avatar: {
        width: 120,
        height: 120,
        borderRadius: 42,
        marginBottom: 16,
    },
    avatarName: {
        fontSize: 24,
        fontFamily: 'ubuntuBold',
        color: colors.textWhite,
    },
    avatarProfession: {
        fontSize: 14,
        fontFamily: 'ubuntuRegular',
        color: colors.backgroundLight,
    },
    main: {
        alignItems: 'center',
        marginTop: -64,
    },
    content: {
        borderColor: colors.backgroundPrimary,
        borderWidth: 1,
        width: '100%',
        padding: 20,
        backgroundColor: colors.backgroundWhite,
        borderRadius: 28,
        marginBottom: 16,
    },
    contentTitle: {
        fontSize: 28,
        fontFamily: 'ubuntuBold',
        marginBottom: 20,
        color: colors.textDark,
        alignSelf: 'center',
    },
    contentProjects: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    project: {
        alignItems: 'center',
        marginHorizontal: 10,
    },
    projectImage: {
        width: 42,
        height: 42,
        marginBottom: 6,
    },
    projectName: {
        fontSize: 14,
        fontFamily: 'ubuntuMedium',
    },
    socialMedia: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 12,
    },
    socialMediaIcon: {
        width: 42,
        height: 42,
        marginRight: 12,
    },
    socialMediaAccount: {
        fontSize: 18,
        fontFamily: 'ubuntuMedium',
    },
})

export default styles

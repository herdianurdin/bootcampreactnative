import * as React from 'react'
import { View, Text, ScrollView, TouchableOpacity, Image } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import styles from './styles'

const About = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <ScrollView
                style={styles.mainWrapper}
                showsVerticalScrollIndicator={false}
                contentInsetAdjustmentBehavior="automatic"
            >
                <View style={styles.headerApp}>
                    <View style={styles.headerTitle}>
                        <TouchableOpacity
                            style={styles.buttonBack}
                            onPress={() => {
                                navigation.navigate('Skills')
                            }}
                        >
                            <MaterialCommunityIcons name="chevron-left" color="white" size={48} />
                        </TouchableOpacity>
                        <Text style={styles.headerTitleText}>About Me</Text>
                    </View>
                    <Image source={require('../../assets/images/avatar.png')} style={styles.avatar} />
                    <Text style={styles.avatarName}>Herdi Herdianurdin</Text>
                    <Text style={styles.avatarProfession}>React Native Developer</Text>
                </View>
                <View style={styles.wrapper}>
                    <View style={styles.main}>
                        <View style={styles.content}>
                            <Text style={styles.contentTitle}>Portfolio</Text>
                            <View style={styles.contentProjects}>
                                <View style={styles.project}>
                                    <Image
                                        source={require('../../assets/images/github.png')}
                                        style={styles.projectImage}
                                    />
                                    <Text style={styles.projectName}>@herdianurdin</Text>
                                </View>
                                <View style={styles.project}>
                                    <Image
                                        source={require('../../assets/images/gitlab.png')}
                                        style={styles.projectImage}
                                    />
                                    <Text style={styles.projectName}>@herdianurdin</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.content}>
                            <Text style={styles.contentTitle}>Social Media</Text>
                            <View style={styles.socialMedia}>
                                <Image
                                    source={require('../../assets/images/facebook.png')}
                                    style={styles.socialMediaIcon}
                                />
                                <Text style={styles.socialMediaAccount}>@herdianurdin</Text>
                            </View>
                            <View style={styles.socialMedia}>
                                <Image
                                    source={require('../../assets/images/twitter.png')}
                                    style={styles.socialMediaIcon}
                                />
                                <Text style={styles.socialMediaAccount}>@herdianurdin</Text>
                            </View>
                            <View style={styles.socialMedia}>
                                <Image
                                    source={require('../../assets/images/instagram.png')}
                                    style={styles.socialMediaIcon}
                                />
                                <Text style={styles.socialMediaAccount}>@herdianurdin</Text>
                            </View>
                            <View style={styles.socialMedia}>
                                <Image
                                    source={require('../../assets/images/telegram.png')}
                                    style={styles.socialMediaIcon}
                                />
                                <Text style={styles.socialMediaAccount}>@herdianurdin</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default About
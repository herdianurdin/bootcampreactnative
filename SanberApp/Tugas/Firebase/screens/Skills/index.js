import * as React from 'react'
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native'
import styles from './styles'
import colors from '../../assets/colors'
import ProgressBar from '../../components/ProgressBar'

const Skills = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <ScrollView
                style={styles.main}
                showsVerticalScrollIndicator={false}
                contentInsetAdjustmentBehavior="automatic"
            >
                <View style={styles.headerApp}>
                    <Text style={styles.headerTitle}>Skills</Text>
                    <Text style={styles.headerSubTitle}>Herdi Herdianurdin</Text>
                </View>
                <View style={styles.contentWrapper}>
                    <View style={styles.content}>
                        <View style={styles.contentHeader}>
                            <Text style={styles.contentTitle}>Programming</Text>
                            <Text style={styles.contentTitle}>Language</Text>
                        </View>
                        <View style={styles.contentProgress}>
                            <View style={styles.contentProgressHeader}>
                                <Image
                                    source={require('../../assets/images/javascript.png')}
                                    style={styles.contentImage}
                                />
                                <Text style={styles.contentSubTitle}>Basic Javascript</Text>
                            </View>
                            <ProgressBar bgColor={colors.javascript} completed={`75%`} />
                        </View>
                        <View style={styles.contentProgress}>
                            <View style={styles.contentProgressHeader}>
                                <Image source={require('../../assets/images/python.png')} style={styles.contentImage} />
                                <Text style={styles.contentSubTitle}>Basic Python</Text>
                            </View>
                            <ProgressBar bgColor={colors.python} completed={`65%`} />
                        </View>
                    </View>
                </View>
                <View style={styles.contentWrapper}>
                    <View style={styles.content}>
                        <View style={styles.contentHeader}>
                            <Text style={styles.contentTitle}>Framework</Text>
                        </View>
                        <View style={styles.contentProgress}>
                            <View style={styles.contentProgressHeader}>
                                <Image source={require('../../assets/images/react.png')} style={styles.contentImage} />
                                <Text style={styles.contentSubTitle}>Basic ReactNative</Text>
                            </View>
                            <ProgressBar bgColor={colors.react} completed={`5%`} />
                        </View>
                        <View style={styles.contentProgress}>
                            <View style={styles.contentProgressHeader}>
                                <Image
                                    source={require('../../assets/images/tensorflow.png')}
                                    style={styles.contentImage}
                                />
                                <Text style={styles.contentSubTitle}>Basic TensorFlow</Text>
                            </View>
                            <ProgressBar bgColor={colors.tensorflow} completed={`8%`} />
                        </View>
                    </View>
                </View>
                <View style={styles.contentWrapper}>
                    <View style={styles.content}>
                        <View style={styles.contentHeader}>
                            <Text style={styles.contentTitle}>Technology</Text>
                        </View>
                        <View style={styles.contentImages}>
                            <Image
                                source={require('../../assets/images/terminal.png')}
                                style={styles.contentImageTech}
                            />
                            <Image
                                source={require('../../assets/images/android.png')}
                                style={styles.contentImageTech}
                            />
                            <Image source={require('../../assets/images/vscode.png')} style={styles.contentImageTech} />
                            <Image source={require('../../assets/images/github.png')} style={styles.contentImageTech} />
                            <Image source={require('../../assets/images/gitlab.png')} style={styles.contentImageTech} />
                        </View>
                    </View>
                </View>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('About')
                    }}
                >
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>About Me</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('Login')
                    }}
                >
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>Logout</Text>
                    </View>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

export default Skills

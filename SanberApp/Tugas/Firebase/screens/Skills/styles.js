import { StyleSheet } from 'react-native'
import colors from '../../assets/colors'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.backgroundWhite,
    },
    main: {
        width: '100%',
    },
    headerApp: {
        backgroundColor: colors.backgroundPrimary,
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        paddingVertical: 30,
        marginBottom: 32,
    },
    headerTitle: {
        fontSize: 32,
        fontFamily: 'ubuntuBold',
        color: colors.textWhite,
    },
    headerSubTitle: {
        fontSize: 24,
        fontFamily: 'ubuntuMedium',
        color: colors.textWhite,
    },
    contentWrapper: {
        paddingHorizontal: 28,
    },
    content: {
        width: '100%',
        alignSelf: 'center',
        padding: 20,
        borderColor: colors.backgroundPrimary,
        borderWidth: 1,
        borderRadius: 28,
        marginBottom: 16,
        marginHorizontal: 36,
    },
    contentHeader: {
        marginBottom: 10,
    },
    contentTitle: {
        fontSize: 28,
        fontFamily: 'ubuntuBold',
        textAlign: 'center',
        color: colors.textDark,
    },
    contentProgress: {
        marginTop: 10,
    },
    contentProgressHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 12,
        marginBottom: 3,
    },
    contentImage: {
        width: 16,
        height: 16,
    },
    contentSubTitle: {
        fontSize: 16,
        fontFamily: 'ubuntuMedium',
        marginLeft: 8,
    },
    contentImages: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    contentImageTech: {
        width: 32,
        height: 32,
        marginHorizontal: 10,
    },
    button: {
        alignItems: 'center',
        marginBottom: 16,
        justifyContent: 'center',
        alignSelf: 'center',
        width: '100%',
        paddingHorizontal: 28,
    },
    buttonText: {
        width: '100%',
        height: 54,
        backgroundColor: colors.backgroundPrimary,
        borderRadius: 18,
        fontSize: 18,
        fontFamily: 'ubuntuBold',
        color: colors.textWhite,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
})

export default styles

import React, { useContext, useState } from 'react'
import { StyleSheet, View, Button, TextInput } from 'react-native'
import { MovieContext } from './MovieContext'

export default MovieForm = () => {
    const [name, setName] = useState('')
    const [lengthOfTime, setLengthOfTime] = useState(0)
    const [movie, setMovie] = useContext(MovieContext)
    const styles = StyleSheet.create({
        formInput: {
            alignItems: 'center',
            borderColor: '#eaeaea',
            borderRadius: 4,
            color: '#7d7d7d',
            flexDirection: 'row',
            fontSize: 18,
            height: 54,
            justifyContent: 'center',
            marginVertical: 8,
            paddingHorizontal: 10,
        },
    })

    const handleSubmit = () => {
        setMovie([...movie, { id: Math.random(), name, lengthOfTime }])
        setName('')
        setLengthOfTime('')
    }
    const handleChangeName = (text) => {
        setName(text)
    }

    const handleChangeTime = (text) => {
        setLengthOfTime(text)
    }

    return (
        <View>
            <TextInput
                style={styles.formInput}
                value={name}
                onChangeText={handleChangeName}
                placeholder="Input Movie Title"
            />
            <TextInput
                style={styles.formInput}
                value={lengthOfTime}
                keyboardType="numeric"
                onChangeText={handleChangeTime}
                placeholder="Input Movie Length"
            />
            <Button title="Submit" onPress={handleSubmit} />
        </View>
    )
}

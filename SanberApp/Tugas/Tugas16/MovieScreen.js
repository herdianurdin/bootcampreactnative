import React from 'react'
import { MovieProvider } from './MovieContext'
import MovieList from './MovieList'
import MovieForm from './MovieForm'

export default MovieScreen = () => {
    return (
        <MovieProvider>
            <MovieList />
            <MovieForm />
        </MovieProvider>
    )
}

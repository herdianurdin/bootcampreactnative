import React, { useContext } from 'react'
import { StyleSheet, Text, View, FlatList } from 'react-native'
import { MovieContext } from './MovieContext'

export default MovieList = () => {
    const [movie] = useContext(MovieContext)
    const styles = StyleSheet.create({
        listContainer: {
            borderWidth: 1,
            borderRadius: 8,
            borderColor: '#eaeaea',
            padding: 10,
            marginHorizontal: 10,
            marginVertical: 5,
        },
    })
    const render = ({ item }) => {
        return (
            <View style={styles.listContainer}>
                <Text>Name : {item.name}</Text>
                <Text>Length of Time : {item.lengthOfTime}</Text>
            </View>
        )
    }

    return <FlatList data={movie} renderItem={render} />
}

import React, { useState, createContext } from 'react'

const MovieContext = createContext()
const MovieProvider = ({ children }) => {
    const [movie, setMovie] = useState([
        { name: 'Harry Potter', lengthOfTime: 120 },
        { name: 'Sherlock Holmes', lengthOfTime: 125 },
        { name: 'Avengers', lengthOfTime: 130 },
        { name: 'Spiderman', lengthOfTime: 124 },
    ])

    return <MovieContext.Provider value={[movie, setMovie]}>{children}</MovieContext.Provider>
}

export { MovieContext, MovieProvider }

import React, { useState } from 'react'
import { Image, StyleSheet, Text, View, TextInput, Button } from 'react-native'

export default function Login({ navigation }) {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [isError, setIsError] = useState(false)

    const submit = () => {
        if (password != '12345678') {
            return () => setIsError(true), alert('Password : 12345678')
        }

        return navigation.navigate('Home', { username: username })
    }
    return (
        <View style={styles.container}>
            <Text style={styles.title}>== Quiz 3 ==</Text>
            <Image style={styles.icon} source={require('./assets/logo.jpg')} />
            <View>
                <TextInput
                    style={styles.input}
                    placeholder='Masukan Username'
                    value={username}
                    onChangeText={(value) => setUsername(value)}
                />
                <TextInput
                    style={styles.input}
                    placeholder='Masukan Password'
                    value={password}
                    onChangeText={(value) => setPassword(value)}
                />
                <Button onPress={submit} title='Login' />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    icon: {
        height: 150,
        width: 150,
    },
    input: {
        borderWidth: 1,
        paddingVertical: 10,
        borderRadius: 5,
        width: 300,
        marginBottom: 10,
        paddingHorizontal: 10,
    },
})
import React from 'react'
import { useState } from 'react'
import { StyleSheet, FlatList, Text, View, Image, Button } from 'react-native'
import { Data } from './data'

const Home = ({ route }) => {
    const { username } = route.params
    const [totalPrice, setTotalPrice] = useState(0)

    const currencyFormat = (num) => {
        return `Rp ${num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`
    }

    const updateHarga = (price) => {
        const temp = Number(price) + totalPrice
        setTotalPrice(temp)
    }

    const renderListItem = ({ item }) => {
        return (
            <View style={styles.content}>
                <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemTitle}>
                    {item.title}
                </Text>

                <Image source={item.image} style={styles.itemImage} resizeMode="contain" />

                <Text style={styles.itemPrice}>{currencyFormat(Number(item.harga))}</Text>

                <Text style={styles.itemName}>{item.type}</Text>

                <Button title="beli" onPress={() => updateHarga(item.harga)} />
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 16 }}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <View style={styles.contentWrapper}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={Data}
                    keyExtractor={(item) => item.id}
                    numColumns={2}
                    renderItem={renderListItem}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    contentWrapper: {
        alignItems: 'center',
        marginBottom: 30,
        paddingBottom: 60,
    },
    content: {
        width: 150,
        height: 220,
        margin: 5,
        borderWidth: 1,
        alignItems: 'center',
        borderRadius: 5,
        borderColor: 'grey',
    },
    itemImage: {
        height: 100,
        width: 100,
    },
    itemTitle: {
        fontWeight: 'bold',
    },
    itemPrice: {
        color: 'black',
        fontSize: 20,
    },
})

export default Home

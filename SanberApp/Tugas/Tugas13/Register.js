import * as React from 'react'
import { View, Text, Image, TextInput, KeyboardAvoidingView, TouchableOpacity, StyleSheet } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import colors from './assets/colors'

export default Register = ({ navigation }) => {
    const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.backgroundWhite,
            alignItems: 'center',
        },
        headerApp: {
            width: '100%',
            marginTop: 80,
            flexDirection: 'row',
            justifyContent: 'center',
            marginBottom: 48,
        },
        headerLogo: {
            width: 64,
            height: 64,
        },
        headerText: {
            marginLeft: 16,
        },
        title: {
            fontFamily: 'ubuntuMedium',
            fontSize: 32,
            lineHeight: 32,
            color: colors.textDark,
        },
        formInput: {
            width: 300,
            marginBottom: 16,
        },
        inputText: {
            fontFamily: 'ubuntuMedium',
            fontSize: 16,
            width: '100%',
            backgroundColor: colors.backgroundLight,
            color: colors.textMuted,
            paddingVertical: 8,
            paddingHorizontal: 28,
            borderRadius: 18,
            marginBottom: 20,
        },
        inputPassword: {
            fontFamily: 'ubuntuMedium',
            fontSize: 16,
            width: '100%',
            backgroundColor: colors.backgroundLight,
            color: colors.textMuted,
            paddingVertical: 8,
            paddingLeft: 28,
            paddingRight: 64,
            borderRadius: 18,
            marginBottom: 20,
        },
        buttonEye: {
            position: 'absolute',
            right: 28,
            top: 10,
        },
        buttonRegister: {
            width: 300,
            alignItems: 'center',
            backgroundColor: colors.backgroundPrimary,
            borderRadius: 18,
            marginBottom: 20,
            height: 54,
            justifyContent: 'center',
        },
        buttonText: {
            fontSize: 18,
            fontFamily: 'ubuntuBold',
            color: colors.textWhite,
        },
        footerApp: {
            position: 'absolute',
            bottom: 41,
            flexDirection: 'row',
        },
        footerText: {
            color: colors.textDark,
            fontSize: 14,
            fontFamily: 'ubuntuRegular',
        },
        footerTextLink: {
            fontFamily: 'ubuntuBold',
            color: colors.textPrimary,
        },
    })
    const [showPassword, setShowPassword] = React.useState(false)
    const [showConfirmPassword, setShowConfirmPassword] = React.useState(false)
    const [visiblePassword, setVisiblePassword] = React.useState(true)
    const [visibleConfirmPassword, setVisibleConfirmPassword] = React.useState(true)

    return (
        <View style={styles.container}>
            <View style={styles.headerApp}>
                <Image source={require('./assets/images/Port.png')} style={styles.headerLogo} />
                <View style={styles.headerText}>
                    <Text style={styles.title}>Create new</Text>
                    <Text style={styles.title}>account!</Text>
                </View>
            </View>
            <View style={styles.formInput}>
                <KeyboardAvoidingView>
                    <TextInput placeholder={'Username'} style={styles.inputText} />
                </KeyboardAvoidingView>
                <KeyboardAvoidingView>
                    <TextInput placeholder={'Email'} style={styles.inputText} />
                </KeyboardAvoidingView>
                <KeyboardAvoidingView>
                    <TextInput
                        secureTextEntry={visiblePassword}
                        placeholder={'Password'}
                        style={styles.inputPassword}
                    />
                    <TouchableOpacity
                        style={styles.buttonEye}
                        onPress={() => {
                            setVisiblePassword(!visiblePassword)
                            setShowPassword(!showPassword)
                        }}
                    >
                        <MaterialCommunityIcons
                            name={!showPassword ? 'eye' : 'eye-off'}
                            size={24}
                            color={colors.textMuted}
                        />
                    </TouchableOpacity>
                </KeyboardAvoidingView>
                <KeyboardAvoidingView>
                    <TextInput
                        secureTextEntry={visibleConfirmPassword}
                        placeholder={'Confirm Password'}
                        style={styles.inputPassword}
                    />
                    <TouchableOpacity
                        style={styles.buttonEye}
                        onPress={() => {
                            setVisibleConfirmPassword(!visibleConfirmPassword)
                            setShowConfirmPassword(!showConfirmPassword)
                        }}
                    >
                        <MaterialCommunityIcons
                            name={!showConfirmPassword ? 'eye' : 'eye-off'}
                            size={24}
                            color={colors.textMuted}
                        />
                    </TouchableOpacity>
                </KeyboardAvoidingView>
            </View>
            <TouchableOpacity
                onPress={() => {
                    navigation.navigate('Login')
                }}
            >
                <View style={styles.buttonRegister}>
                    <Text style={styles.buttonText}>Register</Text>
                </View>
            </TouchableOpacity>
            <View style={styles.footerApp}>
                <Text style={styles.footerText}>Have an account? </Text>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('Login')
                    }}
                >
                    <Text style={styles.footerTextLink}>Login</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

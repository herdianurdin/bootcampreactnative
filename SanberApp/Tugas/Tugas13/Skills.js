import * as React from 'react'
import { View, Text, ScrollView, Image, TouchableOpacity, StyleSheet } from 'react-native'
import colors from './assets/colors'
import ProgressBar from './components/ProgressBar'

export default Skills = ({ navigation }) => {
    const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.backgroundWhite,
        },
        headerApp: {
            backgroundColor: colors.backgroundPrimary,
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'center',
            paddingVertical: 30,
            marginBottom: 32,
        },
        headerTitle: {
            fontSize: 32,
            fontFamily: 'ubuntuBold',
            color: colors.textWhite,
        },
        headerSubTitle: {
            fontSize: 24,
            fontFamily: 'ubuntuMedium',
            color: colors.textWhite,
        },
        content: {
            width: 320,
            alignSelf: 'center',
            padding: 20,
            borderColor: colors.backgroundPrimary,
            borderWidth: 1,
            borderRadius: 28,
            marginBottom: 16,
        },
        contentHeader: {
            marginBottom: 10,
        },
        contentTitle: {
            fontSize: 28,
            fontFamily: 'ubuntuBold',
            textAlign: 'center',
            color: colors.textDark,
        },
        contentProgress: {
            marginTop: 10,
        },
        contentProgressHeader: {
            flexDirection: 'row',
            alignItems: 'center',
            marginHorizontal: 12,
            marginBottom: 3,
        },
        contentImage: {
            width: 16,
            height: 16,
        },
        contentSubTitle: {
            fontSize: 16,
            fontFamily: 'ubuntuMedium',
            marginLeft: 8,
        },
        contentImages: {
            flexDirection: 'row',
            justifyContent: 'center',
        },
        contentImageTech: {
            width: 32,
            height: 32,
            marginHorizontal: 10,
        },
        buttonAbout: {
            width: 320,
            alignItems: 'center',
            backgroundColor: colors.backgroundPrimary,
            borderRadius: 18,
            marginBottom: 16,
            height: 54,
            justifyContent: 'center',
            alignSelf: 'center',
        },
        buttonText: {
            fontSize: 18,
            fontFamily: 'ubuntuBold',
            color: colors.textWhite,
        },
    })

    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.headerApp}>
                    <Text style={styles.headerTitle}>Skills</Text>
                    <Text style={styles.headerSubTitle}>Herdi Herdianurdin</Text>
                </View>
                <View style={styles.content}>
                    <View style={styles.contentHeader}>
                        <Text style={styles.contentTitle}>Programming</Text>
                        <Text style={styles.contentTitle}>Language</Text>
                    </View>
                    <View style={styles.contentProgress}>
                        <View style={styles.contentProgressHeader}>
                            <Image source={require('./assets/images/javascript.png')} style={styles.contentImage} />
                            <Text style={styles.contentSubTitle}>Basic Javascript</Text>
                        </View>
                        <ProgressBar bgColor={colors.javascript} completed={`75%`} />
                    </View>
                    <View style={styles.contentProgress}>
                        <View style={styles.contentProgressHeader}>
                            <Image source={require('./assets/images/python.png')} style={styles.contentImage} />
                            <Text style={styles.contentSubTitle}>Basic Python</Text>
                        </View>
                        <ProgressBar bgColor={colors.python} completed={`65%`} />
                    </View>
                </View>
                <View style={styles.content}>
                    <View style={styles.contentHeader}>
                        <Text style={styles.contentTitle}>Framework</Text>
                    </View>
                    <View style={styles.contentProgress}>
                        <View style={styles.contentProgressHeader}>
                            <Image source={require('./assets/images/react.png')} style={styles.contentImage} />
                            <Text style={styles.contentSubTitle}>Basic ReactNative</Text>
                        </View>
                        <ProgressBar bgColor={colors.react} completed={`5%`} />
                    </View>
                    <View style={styles.contentProgress}>
                        <View style={styles.contentProgressHeader}>
                            <Image source={require('./assets/images/tensorflow.png')} style={styles.contentImage} />
                            <Text style={styles.contentSubTitle}>Basic TensorFlow</Text>
                        </View>
                        <ProgressBar bgColor={colors.tensorflow} completed={`8%`} />
                    </View>
                </View>
                <View style={styles.content}>
                    <View style={styles.contentHeader}>
                        <Text style={styles.contentTitle}>Technology</Text>
                    </View>
                    <View style={styles.contentImages}>
                        <Image source={require('./assets/images/terminal.png')} style={styles.contentImageTech} />
                        <Image source={require('./assets/images/android.png')} style={styles.contentImageTech} />
                        <Image source={require('./assets/images/vscode.png')} style={styles.contentImageTech} />
                        <Image source={require('./assets/images/github.png')} style={styles.contentImageTech} />
                        <Image source={require('./assets/images/gitlab.png')} style={styles.contentImageTech} />
                    </View>
                </View>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('About')
                    }}
                >
                    <View style={styles.buttonAbout}>
                        <Text style={styles.buttonText}>About Me</Text>
                    </View>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

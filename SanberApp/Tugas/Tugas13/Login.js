import * as React from 'react'
import { View, Text, Image, TextInput, KeyboardAvoidingView, TouchableOpacity, StyleSheet } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import colors from './assets/colors'

export default Login = ({ navigation }) => {
    const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.backgroundWhite,
            alignItems: 'center',
        },
        headerApp: {
            width: '100%',
            marginTop: 134,
            flexDirection: 'row',
            justifyContent: 'center',
            marginBottom: 48,
        },
        headerLogo: {
            width: 64,
            height: 64,
        },
        headerText: {
            marginLeft: 16,
        },
        title: {
            fontFamily: 'ubuntuMedium',
            fontSize: 32,
            lineHeight: 32,
            color: colors.textDark,
        },
        formInput: {
            width: 300,
            marginBottom: 16,
        },
        inputEmail: {
            fontFamily: 'ubuntuMedium',
            fontSize: 16,
            width: '100%',
            backgroundColor: colors.backgroundLight,
            color: colors.textMuted,
            paddingVertical: 8,
            paddingHorizontal: 28,
            borderRadius: 18,
            marginBottom: 20,
        },
        inputPassword: {
            fontFamily: 'ubuntuMedium',
            fontSize: 16,
            width: '100%',
            backgroundColor: colors.backgroundLight,
            color: colors.textMuted,
            paddingVertical: 8,
            paddingLeft: 28,
            paddingRight: 64,
            borderRadius: 18,
            marginBottom: 20,
        },
        buttonEye: {
            position: 'absolute',
            right: 28,
            top: 10,
        },
        buttonLogin: {
            width: 300,
            alignItems: 'center',
            backgroundColor: colors.backgroundPrimary,
            borderRadius: 18,
            marginBottom: 20,
            height: 54,
            justifyContent: 'center',
        },
        buttonText: {
            fontSize: 18,
            fontFamily: 'ubuntuBold',
            color: colors.textWhite,
        },
        forgotPassword: {
            fontFamily: 'ubuntuMedium',
            fontSize: 16,
            color: colors.textPrimary,
        },
        footerApp: {
            position: 'absolute',
            bottom: 39,
            flexDirection: 'row',
        },
        footerText: {
            color: colors.textDark,
            fontSize: 14,
            fontFamily: 'ubuntuRegular',
        },
        footerTextLink: {
            fontFamily: 'ubuntuBold',
            color: colors.textPrimary,
        },
    })
    const [show, setShow] = React.useState(false)
    const [visible, setVisible] = React.useState(true)

    return (
        <View style={styles.container}>
            <View style={styles.headerApp}>
                <Image source={require('./assets/images/Port.png')} style={styles.headerLogo} />
                <View style={styles.headerText}>
                    <Text style={styles.title}>Welcome</Text>
                    <Text style={styles.title}>back!</Text>
                </View>
            </View>
            <View style={styles.formInput}>
                <KeyboardAvoidingView>
                    <TextInput placeholder={'Email or Username'} style={styles.inputEmail} />
                </KeyboardAvoidingView>
                <KeyboardAvoidingView>
                    <TextInput secureTextEntry={visible} placeholder={'Password'} style={styles.inputPassword} />
                    <TouchableOpacity
                        style={styles.buttonEye}
                        onPress={() => {
                            setVisible(!visible)
                            setShow(!show)
                        }}
                    >
                        <MaterialCommunityIcons name={!show ? 'eye' : 'eye-off'} size={24} color={colors.textMuted} />
                    </TouchableOpacity>
                </KeyboardAvoidingView>
            </View>
            <TouchableOpacity
                onPress={() => {
                    navigation.navigate('Skills')
                }}
            >
                <View style={styles.buttonLogin}>
                    <Text style={styles.buttonText}>Login</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity>
                <View>
                    <Text style={styles.forgotPassword}>Forgot Password?</Text>
                </View>
            </TouchableOpacity>
            <View style={styles.footerApp}>
                <Text style={styles.footerText}>Don't have any account? </Text>
                <TouchableOpacity
                    onPress={() => {
                        navigation.navigate('Register')
                    }}
                >
                    <Text style={styles.footerTextLink}>Register</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

import * as React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import colors from '../../assets/colors'

const ProgressBar = (props) => {
    const { bgColor, completed } = props
    const styles = StyleSheet.create({
        container: {
            width: '100%',
            height: 24,
            padding: 3,
            borderRadius: 60,
            justifyContent: 'center',
            backgroundColor: colors.backgroundLight
        },
        inner: {
            width: completed,
            backgroundColor: bgColor,
            height: 24,
            borderTopLeftRadius: 60,
            borderBottomLeftRadius: 60,
            borderTopRightRadius: completed == '100%' ? 60 : 0,
            borderBottomRightRadius: completed == '100%' ? 60 : 0,
        },
        label: {
            fontSize: 12,
            fontFamily: 'ubuntuBold',
            color: colors.textDark,
            position: 'absolute',
            zIndex: 1,
            alignSelf: 'center',
        },
    })

    return (
        <View style={styles.container}>
            <View style={styles.inner}></View>
            <Text style={styles.label}>{completed}</Text>
        </View>
    )
}

export default ProgressBar

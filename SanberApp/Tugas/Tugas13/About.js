import * as React from 'react'
import { View, Text, ScrollView, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import colors from './assets/colors'

export default About = ({ navigation }) => {
    const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: colors.backgroundWhite,
        },
        headerApp: {
            backgroundColor: colors.backgroundPrimary,
            height: 350,
            alignItems: 'center',
        },
        headerTitle: {
            justifyContent: 'center',
            width: 320,
            marginHorizontal: 32,
            alignItems: 'center',
            marginTop: 32,
            marginBottom: 12,
        },
        buttonBack: {
            position: 'absolute',
            left: 0,
        },
        headerTitleText: {
            fontSize: 32,
            fontFamily: 'ubuntuBold',
            color: colors.textWhite,
        },
        avatar: {
            width: 120,
            height: 120,
            borderRadius: 42,
            marginBottom: 16,
        },
        avatarName: {
            fontSize: 24,
            fontFamily: 'ubuntuBold',
            color: colors.textWhite,
        },
        avatarProfession: {
            fontSize: 14,
            fontFamily: 'ubuntuRegular',
            color: colors.backgroundLight,
        },
        main: {
            alignItems: 'center',
            marginTop: -64,
        },
        content: {
            borderColor: colors.backgroundPrimary,
            borderWidth: 1,
            width: 320,
            padding: 20,
            backgroundColor: colors.backgroundWhite,
            borderRadius: 28,
            marginBottom: 16,
        },
        contentTitle: {
            fontSize: 28,
            fontFamily: 'ubuntuBold',
            marginBottom: 20,
            color: colors.textDark,
            alignSelf: 'center',
        },
        contentProjects: {
            flexDirection: 'row',
            justifyContent: 'center',
        },
        project: {
            alignItems: 'center',
            marginHorizontal: 10,
        },
        projectImage: {
            width: 42,
            height: 42,
            marginBottom: 6,
        },
        projectName: {
            fontSize: 14,
            fontFamily: 'ubuntuMedium',
        },
        socialMedia: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 12,
        },
        socialMediaIcon: {
            width: 42,
            height: 42,
            marginRight: 12,
        },
        socialMediaAccount: {
            fontSize: 18,
            fontFamily: 'ubuntuMedium',
        },
    })

    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.headerApp}>
                    <View style={styles.headerTitle}>
                        <TouchableOpacity
                            style={styles.buttonBack}
                            onPress={() => {
                                navigation.navigate('Skills')
                            }}
                        >
                            <MaterialCommunityIcons name="chevron-left" color="white" size={48} />
                        </TouchableOpacity>
                        <Text style={styles.headerTitleText}>About Me</Text>
                    </View>
                    <Image source={require('./assets/images/avatar.png')} style={styles.avatar} />
                    <Text style={styles.avatarName}>Herdi Herdianurdin</Text>
                    <Text style={styles.avatarProfession}>React Native Developer</Text>
                </View>
                <View style={styles.main}>
                    <View style={styles.content}>
                        <Text style={styles.contentTitle}>Portfolio</Text>
                        <View style={styles.contentProjects}>
                            <View style={styles.project}>
                                <Image
                                    source={require('./assets/images/github.png')}
                                    style={styles.projectImage}
                                />
                                <Text style={styles.projectName}>@herdianurdin</Text>
                            </View>
                            <View style={styles.project}>
                                <Image
                                    source={require('./assets/images/gitlab.png')}
                                    style={styles.projectImage}
                                />
                                <Text style={styles.projectName}>@herdianurdin</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.contentTitle}>Social Media</Text>
                        <View style={styles.socialMedia}>
                            <Image
                                source={require('./assets/images/facebook.png')}
                                style={styles.socialMediaIcon}
                            />
                            <Text style={styles.socialMediaAccount}>@herdianurdin</Text>
                        </View>
                        <View style={styles.socialMedia}>
                            <Image
                                source={require('./assets/images/twitter.png')}
                                style={styles.socialMediaIcon}
                            />
                            <Text style={styles.socialMediaAccount}>@herdianurdin</Text>
                        </View>
                        <View style={styles.socialMedia}>
                            <Image
                                source={require('./assets/images/instagram.png')}
                                style={styles.socialMediaIcon}
                            />
                            <Text style={styles.socialMediaAccount}>@herdianurdin</Text>
                        </View>
                        <View style={styles.socialMedia}>
                            <Image
                                source={require('./assets/images/telegram.png')}
                                style={styles.socialMediaIcon}
                            />
                            <Text style={styles.socialMediaAccount}>@herdianurdin</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

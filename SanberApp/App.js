import * as React from 'react'
import Tugas13 from './Tugas/Tugas13'
import RestApi from './Tugas/Tugas14/RestApi'
import Tugas15 from './Tugas/Tugas15'
import Quiz3 from './Tugas/Quiz3'
import MovieScreen from './Tugas/Tugas16/MovieScreen'
import FirebaseApp from './Tugas/Firebase'

export default App = () => {
    return <FirebaseApp />
}

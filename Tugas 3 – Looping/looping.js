// Soal 1 (Looping While)
console.log("Soal 1 (Looping While)");

var i = 2;
var j = 20;

console.log("LOOPING PERTAMA");

while (i <= 20) {
    console.log(`${i} - I love coding`);
    i += 2;
}

console.log("LOOPING KEDUA");

while (j >= 2) {
    console.log(`${j} - I will become a mobile developer`);
    j -= 2;
}

console.log();

// Soal 2 (Looping For)
console.log("Soal 2 (Looping For)");

for (var n = 1; n <= 20; n++) {
    console.log(`${n} -`, n % 2 == 1 ? (n % 3 == 0 ? "I Love Coding" : "Santai") : "Berkualitas");
}

console.log();

// Soal 3 (Membuat Persegi Panjang)
console.log("Soal 3 (Membuat Persegi Panjang)");

// Faster Solution
// for (var y = 0; y < 4; y++) {
//     console.log('########')
// }

var length = 8;
var wide = 4;
var tmp = "";

for (var y = 0; y < wide; y++) {
    for (var x = 0; x < length; x++) {
        tmp += "#";
    }

    tmp += "\n";
}

console.log(tmp);

// Soal 4 (Membuat Tangga)
console.log("Soal 4 Membuat Tangga");

var h = 7;
var ladder = "";

for (var y = 0; y < h; y++) {
    for (var x = 0; x <= y; x++) {
        ladder += "#";
    }

    ladder += "\n";
}

console.log(ladder);

// Soal 5 (Membuat Papan Catur)
console.log("Soal 5 (Membuat Papan Catur");

var row = 8;
var col = 8;
var tiles = "";

// Solution 1 => Gunain If Else, tapi kepanjangan

// Solution 2 => Ternary Biasa
// for (var y = 0; y < row; y++) {
//     for (var x = 0; x < col; x++) {
//         tiles += y % 2 == 0 ? (x % 2 == 0 ? ' ' : '#') : (x % 2 == 1 ? ' ' : '#')
//     }

//     tiles += '\n'
// }

// Solution 3 => Ternary Pemalas (XOR)
for (var y = 0; y < row; y++) {
    for (var x = 0; x < col; x++) {
        tiles += (y % 2 == 0) ^ (x % 2 == 0) ? "#" : " ";
    }

    tiles += "\n";
}

console.log(tiles);

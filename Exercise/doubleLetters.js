/*
 * Question!
 * Create a function that takes a word and returns (true)
 * if the word has two consecutive identical letters.
*/

const doubleLetters = (str) => {
    for (let i = 0; i < str.length; i++) {
        for (let j = 0; j < str.length; j++) {
            if (i != j && str[i] == str[j] && i == j + 1)
                return true
        }
    }

    return false
}

console.log(doubleLetters("loop")) // true
console.log(doubleLetters("yummy")) // true
console.log(doubleLetters("orange")) // false
console.log(doubleLetters("munchkin")) // false

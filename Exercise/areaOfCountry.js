/*
 * Question!
 * Create a function that takes a country's (name) and
 * its area as arguments and returns the area of the
 * country's proportion of the total world's landmass.
 *
 * Notes!
 * 1. The total world's landmass is 149.940.000 Km^2
 * 2. Round the result to two decimal places.
*/

const areaOfCountry = (name, area) => {
    let areaPercent = (area * 100 / 149940000).toFixed(2)

    return `${name} is ${areaPercent}% of the total world's landmass`
}

console.log(areaOfCountry("Russia", 17098242))
console.log(areaOfCountry("USA", 9372610))
console.log(areaOfCountry("Iran", 1648195))

/*
 * First output :
 *  "Russia is 11.48% of the total world's landmass"
 * Second output :
 *  "USA is 6.29% of the total world's landmass"
 * Thrid output :
 *  "Iran is 1.11% of the total world's landmass"
*/

/*
 * Question!
 * Create a function that takes a "base number" as an
 * argument. This function should return another function
 * which takes a new argument, and returns the sum of the
 * "base number" and the new argument.
 *
 * Notes!
 * All inputs will be valid numbers
*/

const makePlusFunction = (baseNumber) => {
    const result = (n) => {
        return baseNumber + n
    }

    return result
}

const plusFive = makePlusFunction(5)
console.log(plusFive(2)) // 7
console.log(plusFive(-8)) // -3

const plusTen = makePlusFunction(10)
console.log(plusTen(0)) // 10
console.log(plusTen(188)) // 198
console.log(plusFive(plusTen(0))) // 15

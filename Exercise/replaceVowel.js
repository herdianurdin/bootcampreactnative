/*
 * Question!
 * Create a function that takes a string and replaces the
 * vowels with another character.
 * a = 1, e = 2, i = 3, o = 4, u = 5
 * 
 * Notes!
 * The input will always be in lowercase.
*/

const replaceVowel = (str) => {
    str = str.split('')

    str.forEach((s, i) => {
        switch (s) {
            case 'a':
                str[i] = 1
                break
            case 'e':
                str[i] = 2
                break
            case 'i':
                str[i] = 3
                break
            case 'o':
                str[i] = 4
                break
            case 'u':
                str[i] = 5
                break
        }
    })

    return str.join('')
}

console.log(replaceVowel("karachi")) // "k1r1ch3"
console.log(replaceVowel("chembur")) // "ch2mb5r"
console.log(replaceVowel("khandbari")) // "kh1ndb1r3"

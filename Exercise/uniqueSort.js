/*
 * Question!
 * Given an array of numbers, write a function that returns
 * an array that...
 * 1. Has all duplicate elements removed.
 * 2. Is sorted from least to greatest value
*/

const uniqueSort = (arr) => {
    return [...new Set(arr)].sort((a, b) => a - b)
}

console.log(uniqueSort([1, 2, 4, 3])) // [1, 2, 3, 4]
console.log(uniqueSort([1, 4, 4, 4, 4, 4, 3, 2, 1, 2])) // [1, 2, 3, 4]
console.log(uniqueSort([6, 7, 3, 2, 1])) // [1, 2, 3, 6, 7]

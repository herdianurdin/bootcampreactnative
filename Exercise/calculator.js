/*
 * Question!
 * Create a function that takes two numbers and a mathematical
 * operator (+, -, /, *) and will perform a calculation with
 * the given numbers.
 *
 * Notes!
 * if the input tries to divide by 0, return "Can't divide by 0!"
*/

// Solution 1 => Using eval()
//const calculator = (a, operator, b) => {
//    const result = (operator == '/') && (b == 0) ? `Can't divide by 0!` : eval(`${a} ${operator} ${b}`)

//    return result
//}

// Solution 2 => Native
const calculator = (a, operator, b) => {
    let result = 0

    if (operator == '+') {
        result = a + b
    } else if (operator == '-') {
        result = a - b
    } else if (operator == '*') {
        result = a * b
    } else if (operator == '/') {
        if (b == '0') {
            return `Can't divide by 0!`
        }

        result = a / b
    }

    return result
}

console.log(calculator(1, "/", 0)) // Can't divide by 0!
console.log(calculator(2, "+", 2)) // 4
console.log(calculator(2, "*", 2)) // 4
console.log(calculator(4, "/", 2)) // 2

/*
 * Question!
 * Write a function that takes coordinates of two points on
 * two-dimensional plane and returns the length of the line
 * segment connecting those two points.
 *
 * Notes!
 * 1. The order of the given number is X, Y
 * 2. This challenge is easier than it looks
 * 3. Round your result to two decimal places
*/

const lineLength = (a, b) => {
    const result = Math.hypot(b[0] - a[0], b[1] - a[1])

    return Math.floor(result) != result ? result.toFixed(2) : result
}

console.log(lineLength([15, 7], [22, 11])) // 8.06
console.log(lineLength([0, 0], [0, 0])) // 0
console.log(lineLength([0, 0], [1, 1])) // 1.41

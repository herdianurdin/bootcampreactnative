/*
 * Question!
 * Create a function that returns the mean of all digits.
 *
 * Notes!
 * 1. The mean of all digits is the sum of digits / how
 * many digits there are (e.g. mean of digits in 512
 * (5+1+2)/(number of digits) = 8/3 = 2
 * 2. The mean will always be an integer
*/

const mean = (num) => {
    const nums = []

    while (num) {
        let digit = num % 10
        num = num / 10 | 0

        nums.push(digit)
    }

    return Math.floor(nums.reduce((a, b) => a + b) / nums.length)
}

//const mean = (num) => {
//    num = [...num+''].map(n => +n)

//    return Math.floor(num.reduce((t, c) => t + c) / num.length)
//}

console.log(mean(42)) // 3
console.log(mean(12345)) // 3
console.log(mean(666)) // 6

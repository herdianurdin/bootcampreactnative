/*
 * Question!
 * Write a function that converts an object into an array,
 * where each element represents a key-value pair in the
 * form of an array.
 *
 * Notes!
 * Return an empty array if the object is empty
*/

const toArray = (obj) => {
    const result = []

    for (const [key, val] of Object.entries(obj)) {
        result.push([key, val])
    }

    return result
}

console.log(toArray({ a: 1, b: 2 })) // [["a", 1], ["b", 2]]
console.log(toArray({ shrimp: 15, tots: 12 })) // [["shrimp", 15], ["tots", 12]]
console.log(toArray({})) // []

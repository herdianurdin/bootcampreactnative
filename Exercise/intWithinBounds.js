/*
 * Question!
 * Create a function that validates whether a number (n) is
 * within the bounds of (lower) and (upper). Return false
 * if (n) is not an integer.
 *
 * Notes!
 * 1. The  term "within bounds" mean a number is considered
 * equal or greater than a lower bound and lesser (but not
 * equal) to an upper bound.
 * 2. Bounds will be always given as integers
*/

const intWithinBounds = (n, lower, upper) => {
    if (!Number.isInteger(n) || lower >= upper) {
        return false
    }

    return n >= lower && n < upper
}

console.log(intWithinBounds(3, 1, 9)) // true
console.log(intWithinBounds(6, 1, 6)) // false
console.log(intWithinBounds(4.5, 3, 8)) // false

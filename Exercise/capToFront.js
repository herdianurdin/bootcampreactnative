/*
 * Question!
 * Create a function that moves all capital letters to
 * the front of a word.
 *
 * Notes!
 * Keep the original relative order of the upper and
 * lower case letters the same.
*/

// Using regex
const capToFront = (str) => {
    const upperRegex = /[A-Z]/g
    const lowerRegex = /[a-z]/g
    const upper = str.match(upperRegex)
    const lower = str.match(lowerRegex)

    return [...upper, ...lower].join('')
}

console.log(capToFront("hApPy")) // "APhpy"
console.log(capToFront("moveMENT")) // "MENTmove"
console.log(capToFront("shOrtCAKE")) // "OCAKEshrt"

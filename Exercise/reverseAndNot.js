/*
 * Question!
 * Write a function that takes an integer (i) and returns an
 * integer with the integer backwards followed by the
 * original integer.
 *
 * Notes!
 * (i) is a non-negative integer.
*/

const reverseAndNot = (i) => {
    i = Math.abs(i).toString()

    return [i.split('').reverse().join(''), i].join('')
}

console.log(reverseAndNot(123)) // 321123
console.log(reverseAndNot(152)) // 251152
console.log(reverseAndNot(123456789)) // 987654321123456789

/*
 * Question!
 * Create a function which return the number of true values
 * there are in an array.
*/

// Native Solution
const countTrue = (arr) => {
    let result = 0

    for (let i = 0; i < arr.length; i++) {
        if (arr[i])
            result++
    }

    return result
}

// Using Array Filter
// const countTrue = (arr) => {
//    return arr.filter(a => a == true).length
//}

console.log(countTrue([true, false, false, true, false])) // 2
console.log(countTrue([false, false, false, false])) // 0
console.log(countTrue([])) // 0

/*
 * Question!
 * Given an array of boxes, create a function that returns
 * the total volume of all those boxes combined together. A
 * box is represented by an array with three elements:
 * length, width, and height.
 * For instance, totalVolume([2, 3, 2], [6, 6, 7], [1, 2, 1])
 * should return 266 since (2x3x2)+(6x6*7)+(1x2x1)=12+252+2=266
 *
 * Notes!
 * 1. You will be give at least one box.
 * 2. Each box will always have three dimensions included.
*/

const totalVolume = (...sizes) => {
    let total = 0

    sizes.forEach((size) => {
        total += size.reduce((total, current) => total*current)
    })

    return total
}

console.log(totalVolume([4, 2, 4], [3, 3, 3], [1, 1, 2], [2, 1, 1])) // 63
console.log(totalVolume([2, 2, 2], [2, 1, 1])) // 10
console.log(totalVolume([1, 1, 1])) // 1

/*
 * Question!
 * Create a function that will remove the letters "a",
 * "b", and "c" from the given string and return the
 * modified version. If the given string does not
 * contain "a", "b", or "c" return null.
 *
 * Notes!
*/

const removeABC = (str) => {
    const regex = /[a-c]/g

    if (!str.match(regex)) {
        return null
    }

    let result = ''
    str.split('').forEach((s) => {
        if (s != 'a' ^ s != 'b' ^ s != 'c')
            result += s
    })

    return result
}

console.log(removeABC("This might be a bit hard")) // "This might e  it hrd"
console.log(removeABC("hello world!")) // null
console.log(removeABC("")) // null

/*
 * Question!
 * Create a function that takes an array of integers and
 * returns the sum of all the integers that have an even
 * index, multiplied by the integer at the last index.
 *
 * Notes!
 * If the array is empty, return 0
*/

const evenLast = (arr) => {
    if (arr.length == 0) {
        return 0
    }

    let result = 0

    for (let i = 0; i < arr.length; i++) {
        result += i % 2 == 0 ? arr[i] : 0
    }

    return result * arr[arr.length - 1]
}

console.log(evenLast([])) // 0
console.log(evenLast([1, 3, 3, 1, 10])) // 140
console.log(evenLast([-11, 3, 3, 1, 10])) // 20

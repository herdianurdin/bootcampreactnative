/*
 * Question!
 * According to the lodash documentation, _.compact creates
 * an array with all falsey values removed. The values false,
 * null, 0, "", undefined, and NaN are falsey.
 * Your task is to build this helper function without using
 * lodsh. You will write a function that recives an array and
 * removes all falsey values.
 *
 * Notes!
 * 1. Do not attempt to import lodash
 * 2. You are simply writing your own version
*/

const compact = (arr) => {
    return arr.filter((a) => typeof a == 'number' && a != 0)
}

console.log(compact([0, 1, false, 2, "", 3])) // [1, 2, 3]

/*
 * Question!
 * Create a function that validates whether three given
 * integers form a Pythagorean triplet. The sum of the
 * squares of the two smallest integers must equal the
 * square of the largest number to be validated
 *
 * Notes!
 * Numbers may not be given in a sorted order.
*/

const isTriplet = (a, b, c) => {
    arr = [a, b, c].sort((x, y) => x - y)

    return Math.sqrt(arr[0] ** 2 + arr[1] ** 2) == arr[2]
}

console.log(isTriplet(3, 4, 5)) // true
// 3² + 4² = 25
// 5² = 25

console.log(isTriplet(13, 5, 12)) // true
// 5² + 12² = 169
// 13² = 169

console.log(isTriplet(1, 2, 3)) // false
// 1² + 2² = 5
// 3² = 9

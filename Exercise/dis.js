/*
 * Question!
 * Create a function that takes two arguments: the orginal
 * price and the discount percentage as integers and
 * returns the final price after the discount.
 *
 * Notes!
 * Your answer should be rounded to two decimal places.
*/

const dis = (price, discount) => {
    return parseFloat(price * discount / 100)
}

console.log(dis(1500, 50)) // 750
console.log(dis(89, 20)) // 71.2
console.log(dis(100, 75)) // 25

/*
 * Question!
 * Suppose an image can be represented as a 2D array of 0s
 * and 1s. Write a function to reverse an image. Replace the
 * 0s with 1s and vice versa.
*/

const reverseImage = (arr) => {
    return arr.map((a) => {
       return  a.map((e) => !e ? 1 : 0)
    })
}

console.log(reverseImage([
  [1, 0, 0],
  [0, 1, 0],
  [0, 0, 1]
]))
/*[
  [0, 1, 1],
  [1, 0, 1],
  [1, 1, 0]
]
*/

console.log(reverseImage([
  [1, 1, 1],
  [0, 0, 0]
])) 
/*
[
  [0, 0, 0],
  [1, 1, 1]
]
*/

console.log(reverseImage([
  [1, 0, 0],
  [1, 0, 0]
]))

/*
[
  [0, 1, 1],
  [0, 1, 1]
]
*/

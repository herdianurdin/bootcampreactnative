/*
 * Question!
 * Arrays can be mixed with various types. Your task for
 * this challenge is to sum all the number elements in the
 * given array. Create a function that takes an array and
 * returns the sum of all numbers in the array.
*/

const numbersSum = (arr) => {
    let result = 0

    arr.forEach((a) => {
        result += typeof(a) == 'number' ? a : 0
    })

    return result
}

console.log(numbersSum([1, 2, "13", "4", "645"])) // 3
console.log(numbersSum([true, false, "123", "75"])) // 0
console.log(numbersSum([1, 2, 3, 4, 5, true])) // 15

/*
 * Question!
 * Given an array of women and an array of men, either :
 * 1. Return "sizes don't match" if the two arrays have 
 * diefferent sizes.
 * 2. If the sizes match, return an array of pairs, with
 * the first woman paired with the first man, second woman
 * paired with the second man, etc.
*/

const zipIt = (women, men) => {
    if (women.length != men.length) {
        return `sizes don't match`
    }

    const couples = []

    women.forEach((w, i) => {
        couples.push([w, men[i]])
    })

    return couples
}

console.log(zipIt(["Elise", "Mary"], ["John", "Rick"]))
 // [["Elise", "John"], ["Mary", "Rick"]]

console.log(zipIt(["Ana", "Amy", "Lisa"], ["Bob", "Josh"]))
 // "sizes don't match"

console.log(zipIt(["Ana", "Amy", "Lisa"], ["Bob", "Josh", "Tim"]))
 // [["Ana", "Bob"], ["Amy", "Josh"],["Lisa", "Tim"]]

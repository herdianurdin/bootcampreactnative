/*
 * Question!
 * Create a function to find NaN in an array of numbers.
 * The return value should be the index where NaN is found.
 * If NaN is not found in the array, then return -1.
 *
 * Notes!
 * NaN will occur in the input array only once.
*/

const findNaN = (arr) => {
    let result = -1
    let i = 0

    while (i < arr.length && result == -1) {
        result = isNaN(arr[i]) ? i : -1
        i++
    }

    return result
}

console.log(findNaN([1, 2, NaN])) // 2
console.log(findNaN([NaN, 1, 2, 3, 4])) // 0
console.log(findNaN([0, 1, 2, 3, 4])) // -1

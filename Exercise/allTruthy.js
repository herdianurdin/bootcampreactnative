/*
 * Question!
 * Create a function that returns (true) if all paramters are
 * truthy, and (false) otherwise.
 *
 * Notes!
 * 1. Falsy values include false, 0, "", null, undefined, and
 * NaN. everything else is truthy.
 * 2. You will always be supplied with at least one paramter.
*/

const allTruthy = (...args) => {
    return args.every((a) => a ? true : false)
}

console.log(allTruthy(true, true, true)) // true
console.log(allTruthy(true, false, true)) // false
console.log(allTruthy(5, 4, 3, 2, 1, 0)) // false

/*
 * Question!
 * Create a function that takes three values:
 * h => hours
 * m => minutes
 * s => seconds
 * Return the value that's the longest duration.
 *
 * Notes!
 * No two durations will be the same.
*/

const longestTime = (h, m, s) => {
    const x = Math.max(h * 60, m, s / 60)

    if (x == h * 60)
        return h
    else if (x == s / 60)
        return s

    return m
}

console.log(longestTime(1, 59, 3598)) // 1
console.log(longestTime(2, 300, 15000)) // 300
console.log(longestTime(15, 955, 59400)) // 59400

/*
 * Question!
 * Create a function that returns (true) if an asterik (*)
 * inside a box.
 *
 * Notes!
 * The asterisk may be in the array, however, it must be
 * inside the box, if it exists.
*/

const inBox = (arr) => {
    if (arr[0].includes('*') || arr[arr.length - 1].includes('*')) {
        return false
    }

    for (let i = 1; i < arr.length - 1; i++) {
        let tmp = arr[i].split('')

        if (tmp[0] == '*' || tmp[tmp.length - 1] == '*') {
            return false
        }

        tmp = tmp.slice(1, -1).join('')

        if (tmp.includes('*')) {
            return true
        }
    }

    return false
}

console.log(inBox([
  "###",
  "#*#",
  "###"
])) // true

console.log(inBox([
  "####",
  "#* #",
  "#  #",
  "####"
])) // true

console.log(inBox([
  "*####",
  "# #",
  "#  #*",
  "####"
])) // false

console.log(inBox([
  "#####",
  "#   #",
  "#   #",
  "#   #",
  "#####"
])) // false

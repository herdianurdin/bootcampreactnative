/*
 * Question!
 * Create a function that takes numbers b and m as arguments
 * and returns the derivative of the function f(x)=a^b with
 * respect to x evaluated at x=m, where b and m are constants
 *
 * Note!
 * ^ in the context of this challenge means "to the power of",
 * also known as the "exponent" operator
*/

const derivative = (b, m) => {
    return b * m ** (b-1)
}

console.log(derivative(1, 4)) // 1
console.log(derivative(3, -2)) // 12
console.log(derivative(4, -3)) // -108

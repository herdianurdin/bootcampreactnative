/*
 * Question!
 * Given an int, figure out how many ones, threes, nines you could 
 * fit into the number. You must create a class. You will make
 * variables (class.ones, class.threes, class nines) to do this.
 *
 * Notes!
 * 1. This was originally a Python problem.
 * 2. I'd suggest using getters and setter for this.
*/

class OnesThreesNines {
    constructor(n) {
        this.n = n
        this.ones = n
        this.threes = Math.floor(n / 3)
        this.nines = Math.floor(n / 9)
    }
}

let n1 = new OnesThreesNines(5)
console.log(n1.nines) // 0
console.log(n1.ones) // 5
console.log(n1.threes) // 1

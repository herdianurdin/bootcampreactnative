/*
 * Question!
 * Create a function that takes an array of numbers arr,
 * a string str and return an array of numbers as per the
 * following rules :
 * 1. "Asc" returns a sorted array in ascending order.
 * 2. "Des" returns a sorted array in descending order.
 * 3. "None" returns an array without any modification.
*/

const ascDesNone = (arr, str) => {
    switch (str) {
        case 'Asc':
            arr.sort((a, b) => a - b)
            break
        case 'Des':
            arr.sort((a, b) => b - a)
            break
        default:
    }

    return arr
}

console.log(ascDesNone([4, 3, 2, 1], "Asc" )) // [1, 2, 3, 4]
console.log(ascDesNone([7, 8, 11, 66], "Des")) // [66, 11, 8, 7]
console.log(ascDesNone([1, 2, 3, 4], "None")) // [1, 2, 3, 4]

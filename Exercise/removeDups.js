/*
 * Question!
 * Create a function that takes an array of items, removes
 * all duplicate items and returns a new array in the same
 * sequential order as the old array (minus duplicates).
 *
 * Notes!
 * 1. Tests contain arrays with both strings and numbers.
 * 2. Tests are case sensitive.
 * 3. Each array item is unique
*/

const removeDups = (arr) => {
    return arr.filter((item, index, self) => {
        return self.indexOf(item) == index
    })
}

console.log(removeDups([1, 0, 1, 0])) // [1, 0]
console.log(removeDups(["The", "big", "cat"])) // ["The", "big", "cat"]
console.log(removeDups(["John", "Taylor", "John"])) // ["John", "Taylor"]

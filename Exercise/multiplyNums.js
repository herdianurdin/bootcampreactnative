/*
 * Question!
 * Given a string of numbers separted by a comma and space,
 * return the product of the numbers.
 *
 * Notes!
 * Bonus: Try to complet this challenge in one line!
*/

const multiplyNums = (nums) => {
    return nums.split(', ').reduce((t, n) => t * n)
}

console.log(multiplyNums("2, 3")) // 6
console.log(multiplyNums("1, 2, 3, 4")) // 24
console.log(multiplyNums("54, 75, 453, 0")) // 0
console.log(multiplyNums("10, -2")) // -20

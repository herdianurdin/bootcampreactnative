/*
 * Question!
 * Given a total due and an array representing the amount of
 * change in your pocket, determine whether or not you are
 * able to pay for the item. Change will always be represented
 * in the following order: quarters, dimes, nickels, pennies.
 *
 * Notes!
 * 1. quarter = 25 cents
 * 2. dime = 10 cents
 * 3. nickel = 5 cents
 * 4. penny = 1 cents
*/

const changeEnough = (change, amountDue) => {
    const data = [0.25, 0.1, 0.05, 0.01]
    let amount = change.reduce(
        (previousValue, currentValue, currentIndex) => {
            return previousValue + currentValue * data[currentIndex]
        }, 0
    )

    return amount >= amountDue
}

console.log(changeEnough([2, 100, 0, 0], 14.11)) // false
console.log(changeEnough([0, 0, 20, 5], 0.75)) // true
console.log(changeEnough([30, 40, 20, 5], 12.55)) // true
console.log(changeEnough([10, 0, 0, 50], 3.85)) // false
console.log(changeEnough([1, 0, 5, 219], 19.99)) // false

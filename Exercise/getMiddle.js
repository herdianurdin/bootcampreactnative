/*
 * Question!
 * Create a function that takes a string and returns the
 * middle character(s). If the word's length is odd, return
 * the middle character. If the word's length is even, return
 * the middle two characters.
 *
 * Notes!
 * All test cases contain a single word (as a string)
*/

const getMiddle = (str) => {
    if (str.length % 2 == 1) {
        return str[Math.floor(str.length / 2)]
    }

    return str[str.length / 2 - 1] + str[str.length / 2]
}

console.log(getMiddle("test")) // "es"
console.log(getMiddle("testing")) // "t"
console.log(getMiddle("middle")) // "dd"
console.log(getMiddle("A")) // "A"

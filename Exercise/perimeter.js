/*
 * Question1
 * Write a function that takes a number and returns the
 * perimter of either a circle or a square. The input will
 * be in the form (letter l, number num) where the letter
 * will be either "s" for square, or "c" for cirlce, and
 * the number will be the side of the square or the radius
 * of the cirlce.
 * Use following formulas :
 * Perimeter of a square: 4 * side.
 * Perimeter of a circle: 6.28 * radius.
 * The catch is you can only use arithmetic or comparison
 * operators, which means :
 * 1. No if.. else statements.
 * 2. No objects.
 * 3. No arrays.
 * 4. No formatting method, etc
 * The goal is to write a short, branch-free pieces of code.
 *
 * Notes!
 * No rounding is needed.
*/

const perimeter = (l, num) => {
    return l == 's' ? (4 * num) : (6.28 * num)
}

console.log(perimeter("s", 7)) // 28
console.log(perimeter("c", 4)) // 25.12
console.log(perimeter("c", 9)) // 56.52

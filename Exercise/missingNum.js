/*
 * Question!
 * Create a function that takes an array of numbers between 1
 * and 10 (excluding one number) and returns the missing
 * number.
 *
 * Notes!
 * 1. The array of numbers will be unsorted (not in order)
 * 2. Only one number will be missing
*/

const missingNum = (arr) => {
    arr = arr.sort((a, b) => a - b)

    for (let i = 0; i < arr.length - 1; i++) {
        if (arr[i] + 1 != arr[i+1]) {
            return arr[i] + 1
        }
    }

    return arr[arr.length - 1] + 1
}

console.log(missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10])) // 5
console.log(missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8])) // 10
console.log(missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9])) // 7

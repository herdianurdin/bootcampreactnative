/*
 * Question!
 * Your task is to create a Circle constructor that creates
 * a circle with a radius provided by an argument. The circles
 * constructed must have two methods getArea() (PI * r ^ 2)
 * and getPerimeter() (2 * Pi * r) which give both respective
 * areas and perimeter (circumference)
 *
 * Notes!
 * Don't worry about floating point precision, I've factored
 * int the change that your answer may be more or less accurate
 * than mine. This is more of a tutorial than a challenge so the
 * topic covered may be considered advanced, yet the challenge is
 * more simple - so if this challenge gets labelled as easy, don't
 * worry too much
*/

class Circle {
    constructor(r) {
        this.r = r
    }

    getArea() {
        return Math.PI * this.r ** 2
    }

    getPerimeter() {
        return 2 * Math.PI * this.r
    }
}

let circy1 = new Circle(11)
console.log(circy1.getArea()) // 380.132711084365

let circy2 = new Circle(4.44)
console.log(circy2.getPerimeter()) // 27.897342763877365


/*
 * Question!
 * Create a function that takes an array of numbers and returns
 * the sum of the two lowest positive numbers.
 *
 * Notes!
 * 1. Don't count negative numbers.
 * 2. Floats and empty arrays will not be used in any of the test
 * cases.
*/

// Naif Solution
const sumTwoSmallestNums = (arr) => {
    let result = arr[0] + arr[1]

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > 0) {
            for (let j = 0; j < arr.length - 1; j++) {
                if (arr[j] > 0 && j != i) {
                    result = (arr[i] + arr[j] < result) ? (arr[i] + arr[j]) : result
                }
            }
        }
    }

    return result
}

console.log(sumTwoSmallestNums([19, 5, 42, 2, 77])) // 7
console.log(sumTwoSmallestNums([10, 343445353, 3453445, 3453545353453])) // 3453455
console.log(sumTwoSmallestNums([2, 9, 6, -1])) // 8
console.log(sumTwoSmallestNums([879, 953, 694, -847, 342, 221, -91, -723, 791, -587])) // 563
console.log(sumTwoSmallestNums([3683, 2902, 3951, -475, 1617, -2385])) // 4519

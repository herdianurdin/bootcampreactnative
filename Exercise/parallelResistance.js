/*
 * Question!
 * If two or more resistors are connected in parallel, the
 * overall resistance of the circuit reduces. It is possible
 * to calculate the total resistance of a parallel circuit
 * by using this formla
 * 1/rt = 1/r1 + 1/r2 + 1/r3 + ... + 1/rn
 * Create a function that takes an array of parallel resistnace
 * values, and calculates the total resistance of the circuit.
 *
 * Notes!
 * 1. Note that you should rearrage to return the Resistance
 * Total, not 1 / Resistance Total.
 * 2. Round to the nearest decimal place.
 * 3. All inputs will be valid
*/

const parallelResistance = (arr) => {
    const result = 1 / arr.reduce((total, r) => {
        return total + 1 / r
    }, 0)
    
    return (result - Math.floor(result) != 0) ? result.toFixed(1) : result
}

console.log(parallelResistance([6, 3])) // 2
console.log(parallelResistance([10, 20, 10])) // 4
console.log(parallelResistance([500, 500, 500])) // 166.6

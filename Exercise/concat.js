/*
 * Question!
 * Create a function that concatenates (n) input arrays,
 * where (n) is variable
 *
 * Notes!
 * Arrays should be concatenated in order of the arguments
*/

const concat = (...args) => {
    const result = []

    args.forEach((arr) => {
        result.push(...arr)
    })

    return result
}

console.log(concat([1, 2, 3], [4, 5], [6, 7]))
console.log(concat([1], [2], [3], [4], [5], [6], [7]))
console.log(concat([1, 2], [3, 4]))
console.log(concat([4, 4, 4, 4, 4]))

/*
 * First output :
 *  [1, 2, 3, 4, 5, 6, 7]
 * Second output :
 *  [1, 2, 3, 4, 5, 6, 7]
 * Third output :
 *  [1, 2, 3, 4]
 * Fourth output :
 *  [4, 4, 4, 4, 4]
*/

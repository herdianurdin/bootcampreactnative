/*
 * Question!
 * Write a function that does the following for the given
 * values: add, subtract, divide, and multiply. This is
 * simply referred to as the basic arithmetic operations.
 * The variables have to be defined, but in this challenge,
 * it will be defined for you. All you have to do, is to
 * check the variables, do some string to integer conversion,
 * use some if conditions, and apply the arithmetic operation.
 * The numbers and operation are giben as a string and should
 * result to an integer value.
 * 
 * Notes!
 * 1. The numbers an operation are given as a string and
 * should result to an integer value.
 * 2. If the operation results to Infinity, then return
 * "undefined" => divisin by 0
 * 3. Division results will be rounded down its integeral
 * part.
*/

// Native Solution
const operation = (a, b, operator) => {
    a = parseInt(a)
    b = parseInt(b)
    let result = 0

    switch(operator) {
        case 'add':
            result = a + b
            break
        case 'subtract':
            result = a - b
            break
        case 'divide':
            result = b == 0 ? undefined : Math.floor(a / b)
            break
        case 'multiply':
            result = a * b
            break
    }

    return result
}

console.log(operation("1",  "2",  "add" )) // 3
console.log(operation("4",  "5",  "subtract")) // -1
console.log(operation("6",  "3",  "divide")) // 2

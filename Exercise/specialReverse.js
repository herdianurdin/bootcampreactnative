/*
 * Question!
 * Write a function that reverses all the words in a
 * sentence that start with a particular letter
 *
 * Notes!
 * 1. Reverse the words themselves, not the entire
 * sentence.
 * 2. All characters in the sentence will be in lower
 * case.
*/

const specialReverse = (s, c) => {
    s = s.split(' ')

    for (let i = 0; i < s.length ; i++) {
        if (s[i][0] == c) {
            s[i] = s[i].split('').reverse().join('')
        }
    }

    return s.join(' ')
}

console.log(specialReverse("word searches are super fun", "s"))
// "word sehcraes are repus fun"

console.log(specialReverse("first man to walk on the moon", "m"))
// "first nam to walk on the noom"

console.log(specialReverse("peter piper picked pickled peppers", "p"))
// "retep repip dekcip delkcip sreppep"

/*
 * Question!
 * Create a function that takes an array of names and returns
 * an array where only the first letter of each name is
 * capitalized.
 *
 * Notes!
 * 1. Don't change the order of the original array.
 * 2. Notice in the second example above, "MABELLE" is return
 * as "Mabelle".
*/

const capMe = (arr) => {
    for (let i = 0; i < arr.length; i++) {
        arr[i] = arr[i].toLowerCase().split('')
        arr[i][0] = arr[i][0].toUpperCase()
        arr[i] = arr[i].join('')
    }

    return arr
}

console.log(capMe(["mavis", "senaida", "letty"]))
// ["Mavis", "Senaida", "Letty"]
console.log(capMe(["samuel", "MABELLE", "letitia", "meridith"]))
// ["Samuel", "Mabelle", "Letitia", "Meridith"]
console.log(capMe(["Slyvia", "Kristal", "Sharilyn", "Calista"]))
// ["Slyvia", "Kristal", "Sharilyn", "Calista"]

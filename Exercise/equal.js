/*
 * Question!
 * Create a function that takes three integer arguments
 * (a, b, c) and returns the amount of integers which are
 * of equal value.
 *
 * Notes!
 * Your function must return 0, 2, or 3
*/

const equal = (a, b, c) => {
    const count = [...new Set([a, b, c])].length

    return count == 3 ? 0 : count + 1
}

console.log(equal(3, 4, 3)) // 2
console.log(equal(1, 1, 1)) // 3
console.log(equal(3, 4, 1)) // 0

/*
 * Question!
 * Given a number, n, return a function that which adds n to
 * the number passed to it.
 *
 * Notes!
 * 1. All numbers used in the tests will be integers (whole
 * numbers).
 * 2. Returning a function from a function is a key part of
 * understanding higher order functions (functions which 
 * operate on and return functions).
*/

const add = (a) => {
    return (b) => {
        return a + b
    }
}

console.log(add(10)(20)) // 30
console.log(add(0)(20)) // 20
console.log(add(-30)(80)) // 50

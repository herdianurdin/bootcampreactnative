/*
 * Question!
 * Create a function that calculates the missing value of 3
 * inputs using Ohm's law. The inputs are v, r, or i.
 *
 * Notes!
 * 1. Missing values will be ""
 * 2. If there is more than one missing value, or no missing
 * value, return "invalid"
 * 3. Only numbers will be given.
*/

const ohmsLaw = (v, r, i) => {
    if (v && r && i) {
        return "invalid"
    } else if (r && i) {
        return r * i
    } else if (v && r) {
        return v / r
    } else if (v && i) {
        return v / i
    }

    return 'invalid'
}

console.log(ohmsLaw(12, 220, "")) // 0.05
console.log(ohmsLaw(230, "", 2)) // 115
console.log(ohmsLaw("", 220, 0.02)) // 4.4
console.log(ohmsLaw("", "", 10)) // "Invalid"
console.log(ohmsLaw(500, 50, 10)) // "Invalid"

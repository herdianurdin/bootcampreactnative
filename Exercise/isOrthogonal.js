/*
 * Question!
 * Create a function that takes two vectors as arrays
 * and checks if the two vectors are orthogonal or not.
 * The return value is boolean. Two vector a and b are
 * orthogonal if their dot product is equal to zero.
 *
 * Notes!
 * Two arrays will be same length.
*/

const isOrthogonal = (a, b) => {
    let result = 0

    a.forEach((x, i) => {
        result += x * b[i]
    })

    return !result ? true : false
}

console.log(isOrthogonal([1, 2], [2, -1])) // true
console.log(isOrthogonal([3, -1], [7, 5])) // false
console.log(isOrthogonal([1, 2, 0], [2, -1, 10])) // true

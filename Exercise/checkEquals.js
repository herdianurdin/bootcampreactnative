/*
 * Question!
 * Create a function that returns true if two arrays
 * contain identical values, and false otherwise.
*/

const checkEquals = (a, b) => {
    if (a.length != b.length) {
        return false
    }

    // If needed to sort
//    a.sort((x, y) => x - y)
//    b.sort((x, y) => x - y)

    for (let i = 0; i < a.length; i++) {
        if (a[i] != b[i]) {
            return false
        }
    }

    return true
}

console.log(checkEquals([1, 2], [1, 3])) // false
console.log(checkEquals([1, 2], [1, 2])) // true
console.log(checkEquals([4, 5, 6], [4, 5, 6])) // true
console.log(checkEquals([4, 7, 6], [4, 5, 6])) // false
console.log(checkEquals([4, 7, 6], [4, 6, 7])) // false


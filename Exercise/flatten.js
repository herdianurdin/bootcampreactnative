/*
 * Question!
 * I'm trying to write a function to flatten an array of
 * subarrays into one array. (Suppose i am unware there is a
 * .flat() method in the Array prototype). In other words,
 * I want to transform this: [ [1, 2], [3, 4] ] into
 * [1, 2, 3, 4]
*/

// Solution 1 => Using concat
//const flatten = (arr) => {
//    return arr[0].concat(arr[1])
//}

// Solution 2 => args
const flatten = (arr) => {
    return [...arr[0], ...arr[1]]
}

console.log(flatten([[1, 2], [3, 4]]))
console.log(flatten([["a", "b"], ["c", "d"]]))
console.log(flatten([[true, false], [false, false]]))

// Expected: [1, 2, 3, 4]
// Expected: ["a", "b", "c", "d"]
// Expected: [true, false, false, false]

/*
 * Question!
 * Create a function that takes an array of items and
 * check if the last item mathces the rest of the array
 * concatenated together.
 *
 * Notes!
 * The array is always filled with items
*/

const matchLastItem = (arr) => {
    const lastItem = arr[arr.length-1]
    arr.pop()

    return arr.join('') == lastItem
}

console.log(matchLastItem(["rsq", "6hi", "g", "rsq6hig"])) // true
// The last item is the rest joined.

console.log(matchLastItem([1, 1, 1, "11"])) // false
// The last item should be "111".

console.log(matchLastItem([8, "thunder", true, "8thundertrue"])) // true

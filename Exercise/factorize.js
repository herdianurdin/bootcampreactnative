/*
 * Question!
 * Create a function that takes a number as its argument
 * and returns an array of all its factors.
 *
 * Notes!
 * 1. The input integer will be positive.
 * 2. A factor is a number that evenly divides into another
 * number without leaving a remainder. The second example
 * is a factor of 12, because 12 / 2 = 6, with remainder
 * 0.
*/

const factorize = (n) => {
    const result = []

    for (let i = 1; i <= n; i++) {
        if (n % i == 0) result.push(i)
    }

    return result
}

console.log(factorize(12)) // [1, 2, 3, 4, 6, 12]
console.log(factorize(4)) // [1, 2, 4]
console.log(factorize(17)) // [1, 17]

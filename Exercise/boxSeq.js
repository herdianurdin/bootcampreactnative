/*
 * Question!
 * Create a function that takes a number (step) as an
 * argument and returns the amount of boxes in that
 * step of the sequence.
 *
 * Notes!
 * Step (the input) is always a positive integer
*/

const boxSeq = (n) => {
    if (n == 0) {
        return 0
    }

    let result = 0

    for (let i = 1; i <= n; i++) {
        if (i % 2 == 1) {
            result += 3
        } else {
            result -= 1
        }
    }

    return result
}

console.log(boxSeq(0)) // 0
console.log(boxSeq(1)) // 3
console.log(boxSeq(2)) // 2
console.log(boxSeq(3)) // 5
console.log(boxSeq(4)) // 4

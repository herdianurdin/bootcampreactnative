/*
 * Question!
 * An isogram is a word that has no duplicate letters.
 * Create a function that takes a string and returns
 * either (true) or (false) depending on whether or not
 * it's an "isogram".
 *
 * Notes!
 * 1. Ignore letter case (should not be case sensitive)
 * 2. All test cases contain valid one word strings.
*/

const isIsogram = (str) => {
    str = str.toLowerCase()
    let tmp = [... new Set(str.split(''))].join('')

    return str == tmp
}

console.log(isIsogram("Algorism")) // true
console.log(isIsogram("PasSword")) // false
console.log(isIsogram("Consecutive")) // false

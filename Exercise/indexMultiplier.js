/*
 * Question!
 * Return the sum of all items in an array, where each
 * item is multiplied by its index (zero-based). For
 * empty arrays, return 0
 *
 * Notes!
 * All items in the array will be integers.
*/

const indexMultiplier = (arr) => {
    return arr.reduce((p, c, i) => {
        return p + c * i
    }, 0)
}

console.log(indexMultiplier([1, 2, 3, 4, 5]))
// (1*0 + 2*1 + 3*2 + 4*3 + 5*4) = 40

console.log(indexMultiplier([-3, 0, 8, -6]))
// (-3*0 + 0*1 + 8*2 + -6*3) = -2

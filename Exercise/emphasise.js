/*
 * Question!
 * Create a function that returns a capitalized version of
 * the string passed. The first letter of each word in the
 * string should be capitalized while the rest of each word
 * should be lowercase.
*/

const emphasise = (str) => {
    str = str.toLowerCase()
    str = str.split(' ')

    for (let i = 0; i < str.length; i++) {
        str[i] = str[i].split('')
        str[i][0] = str[i][0].toUpperCase()
        str[i] = str[i].join('')
    }

    return str.join(' ')
}

console.log(emphasise("hello world")) // "Hello World"
console.log(emphasise("GOOD MORNING")) // "Good Morning"
console.log(emphasise("99 red balloons!")) // "99 Red Balloons!"

/*
 * Question!
 * A repdigit is a positive number composed out of the same
 * digit. Create a function that takes an integer and returns
 * whether it's a repdigit or not.
 *
 * Notes!
 * The number 0 should return true.
*/

const isRepdigit = (n) => {
    if (n < 0) {
        return false
    }

    n = n.toString().split('')
    n = n.every((c) => c == n[0])

    return n
}

console.log(isRepdigit(66)) // true
console.log(isRepdigit(0)) // true
console.log(isRepdigit(-11)) // false

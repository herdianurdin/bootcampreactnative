/*
 * Question!
 * Create a function that takes a string as an argument and
 * returns a coded (h4ck3r 5p34k) version of the string.
 *
 * Notes!
 * In order to work properly, the function should replace
 * all "a" with 4, "e" with 3, "i" with 1, "o" with 0, and
 * "s" with 5.
*/

const hackerSpeak = (str) => {
    str = str.split('')

    str.forEach((e, i) => {
        switch(e) {
            case('a'):
                str[i] = 4
                break
            case('e'):
                str[i] = 3
                break
            case('i'):
                str[i] = 1
                break
            case('o'):
                str[i] = 0
                break
            case('s'):
                str[i] = 5
                break
        }
    })

    return str.join('')
}

console.log(hackerSpeak("javascript is cool")) // "j4v45cr1pt 15 c00l"
console.log(hackerSpeak("programming is fun")) // "pr0gr4mm1ng 15 fun"
console.log(hackerSpeak("become a coder")) // "b3c0m3 4 c0d3r"

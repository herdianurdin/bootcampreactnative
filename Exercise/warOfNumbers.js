/*
 * Question!
 * There's a great war between the even and odd numbers.
 * Many numbers already lost their life in this war and it's
 * your task to end this. You have to dermine which group
 * sums larger: the even, or the odd. The large group wins.
 * Create a function that takes an array of integers, sums the
 * even and odd number separately, then returns the difference
 * between sum of even and odd numbers.
 *
 * Notes!
 * The given array contains only positive integers.
*/

const warOfNumbers = (arr) => {
    arr = arr.map((e) => Math.abs(e))

    let odd = 0
    let even = 0

    arr.forEach((e) => {
        if (e % 2 == 1) {
            odd += e
        } else {
            even += e
        }
    })

    return odd > even ? (odd - even) : (even - odd)
}

console.log(warOfNumbers([2, 8, 7, 5])) // 2
// 2 + 8 = 10
// 7 + 5 = 12
// 12 is larger than 10
// So we return 12 - 10 = 2
console.log(warOfNumbers([12, 90, 75])) // 27
console.log(warOfNumbers([5, 9, 45, 6, 2, 7, 34, 8, 6, 90, 5, 243])) // 168

/*
 * Question!
 * Closures are functions that remember their lexical environments.
 * Lexical environments mean the environment in which the function
 * was declared.
*/

const greetingMaker = (str) => {
    const result = (name) => {
        return `${str}, ${name}`
    }

    return result
}

const greeting = greetingMaker("Hello")
console.log(greeting("James")) // "Hello, James"

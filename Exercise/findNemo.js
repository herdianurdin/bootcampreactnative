/*
 * Question!
 * You're given a string of words. You need to find the word
 * "Nemo", and return a string like this: "I Found Nemo at
 * [the order of the word you find nemo]!".
 * if you can't find Nemo, return "I can't find Nemo :(".
 *
 * Notes!
 * 1. !, ? . are always seprated from the last word.
 * 2. Nemo will always look like Nemo, and no neMo or other
 * capital variations.
 * 3. Nemo's or anythin that says Nemo with something behind
 * it, doesn't count as Finding Nemo.
 * 4. If there are multiple Nemo's in the sentence, only return
 * for the first one.
*/

const findNemo = (str) => {
    const result = str.split(' ').indexOf('Nemo')
    
    return result == -1 ? `I can't find Nemo :(` : `I found Nemo at ${result + 1}!`
}

console.log(findNemo("I am finding Nemo !")) // "I found Nemo at 4!"
console.log(findNemo("Nemo is me")) // "I found Nemo at 1!"
console.log(findNemo("I Nemo am")) // "I found Nemo at 2!"

/*
 * Question!
 * Mary wants to run a 25-mile marathon. When she attempts
 * to sign up for the marathon, she notices the sign-up
 * sheet doesn't directly state the marathon's length.
 * instead, the marathon's length is listed in small,
 * different portions. Help Mary find out how long the
 * marathon actually is. Return (true) if the marathon is
 * 25 miles long, otherwise, return (false).
 *
 * Notes!
 * 1. Items in the array will always be integers.
 * 2. Items in the array may be negative or positive, but
 * since negetive distance isn't possible, find a way to
 * convert negative integers into positive integers.
 * 3. Return (false) if the arguments are empty or not
 * provided.
*/

const marathonDistance = (arr) => {
    return arr.map((e) => Math.abs(e)).reduce((t, c) => t + c) == 25
}

console.log(marathonDistance([1, 2, 3, 4])) // false
console.log(marathonDistance([1, 9, 5, 8, 2])) // true
console.log(marathonDistance([-6, 15, 4])) // true

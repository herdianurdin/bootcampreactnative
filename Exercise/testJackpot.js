/*
 * Question!
 * Create a function that takes in an array (slot machine
 * outcome) and returns (true) if all elements in the
 * array are identical, and (false) otherwise. The array
 * will contain 4 elements.
 *
 * Notes!
 * The elements mus be exactly identical for there to be
 * a jackpot.
*/

const testJackpot = (arr) => {
    return arr.every((c) => c == arr[0])
}

console.log(testJackpot(["@", "@", "@", "@"])) // true
console.log(testJackpot(["abc", "abc", "abc", "abc"])) // true
console.log(testJackpot(["SS", "SS", "SS", "SS"])) // true
console.log(testJackpot(["&&", "&", "&&&", "&&&&"])) // false
console.log(testJackpot(["SS", "SS", "SS", "Ss"])) // false

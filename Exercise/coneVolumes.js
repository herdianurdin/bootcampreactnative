/*
 * Question!
 * Create a function that takes the height and radius of
 * a cone as arguments and returns the volume of the cone
 * rounded to the nearest hundredth.
 *
 * Notes!
 * 1. Return approximate answer by rounding the answer to
 * the nearest hundredth.
 * 2. Use JavaScript's Pi peroperty, don't fall for 3.14
 * 3. If the cons has no volume, return 0
*/

const coneVolume = (h, r) => {
    const result = 1 / 3 * Math.PI * r ** 2 * h

    return result == 0 ? 0 : result.toFixed(2)
}

console.log(coneVolume(3, 2)) // 12.57
console.log(coneVolume(15, 6)) // 565.49
console.log(coneVolume(18, 0)) // 0

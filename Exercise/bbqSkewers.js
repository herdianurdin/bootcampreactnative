/*
 * Question!
 * You are in charge of the barbecue grill. A vegetarian skewer
 * is a skewer that has only vegetables (-o). A non-vegetarian
 * is a skewer with at least one piece of meat (-x)
 * Create a function returns [totalVegetarian, totalNonVegetarian]
*/

const bbqSkewers = (arr) => {
    let nonVeg = 0

    arr.forEach((a) => {
        if (a.includes('x')) {
            nonVeg++
        }
    })

    return [arr.length - nonVeg, nonVeg]
}

console.log(bbqSkewers( [
  "--oooo-ooo--",
  "--xx--x--xx--",
  "--o---o--oo--",
  "--xx--x--ox--",
  "--xx--x--ox--"
])) // [2, 3]

console.log(bbqSkewers([
  "--oooo-ooo--",
  "--xxxxxxxx--",
  "--o---",
  "-o-----o---x--",
  "--o---o-----"
])) // [3, 2]

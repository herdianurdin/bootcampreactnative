/*
 * Question!
 * Create a function that counts the integer's number of
 * digits.
 *
 * Notes!
 * 1. For an added challenge, try to solve this without
 * using strings.
 * 2. Alternatively, you can solve this with recursive
 * approach.
*/

// Solution 1 => With recurisve
//const count = (n) => {
//    if (n/10 == 0) {
//        return 1
//    }

//    return count(parseInt(n / 10)) + 1
//}

// Solution 2 => Without recursive
const count = (n) => {
    n = Math.abs(n)
    let result = 0

    while (n) {
        n = Math.floor(n / 10)
        result ++
    }

    return result
}

console.log(count(318)) // 3
console.log(count(-92563)) // 5
console.log(count(4666)) // 4
console.log(count(-314890)) // 6
console.log(count(654321)) // 6
console.log(count(638476)) // 6

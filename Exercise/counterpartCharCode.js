/*
 * Question!
 * Create a function that takes a single character as an
 * argument and returns the char code of its lowercased /
 * uppercased counterpart
 *
 * Notes!
 * 1. The argument will always be a single character
 * 2. Not all inputs will have a counterpart (e.g. numbers),
 * in which case return the input char code.
*/

const counterpartCharCode = (chr) => {
    return chr == chr.toUpperCase() ? chr.toLowerCase().charCodeAt(0) : chr.toUpperCase().charCodeAt(0)
}

console.log(counterpartCharCode("A")) // 97
console.log(counterpartCharCode("a")) // 65

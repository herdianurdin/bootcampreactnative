/*
 * Question!
 * Given radius (r) and height (h) in cm, calculate the mass
 * of a cylinder when it's filled with water and the cylinder
 * itself doesn't weigh anything. The desired output should
 * be given in kg and rounded to two decimal places.
 * How to solve?
 * 1. Calculate the volume of the cylinder
 * 2. Convert cm^3 into dm^3
*/

const weight = (r, h) => {
    return parseFloat((Math.PI * r ** 2 * h / 1000).toFixed(2))
}

console.log(weight(4, 10)) // 0.5
console.log(weight(30, 60)) // 169.65
console.log(weight(15, 10)) // 7.07

/*
 * Question!
 * Create a function that returns the thickness (in meters)
 * of piece of paper after folding it (n) number of times.
 * The paper starts off with a thickness of 0.5mm.
 *
 * Notes!
 * 1. There are 1000mm in a single meter.
 * 2. Don't round answers.
*/

const numLayers = (n) => {
    const size = 2 ** n * 0.5

    return `Paper folded ${n} times is ${size}mm (equal to ${size / 1000})`
}

console.log(numLayers(1)) // "0.001m"
// Paper folded once is 1mm (equal to 0.001m)

console.log(numLayers(4)) // "0.008m"
// Paper folded 4 times is 8mm (equal to 0.008m)

console.log(numLayers(21)) // "1048.576m"
// Paper folded 21 times is 1048576mm (equal to 1048.576m)

/*
 * Question!
 * Given a two digit number, return (true) if that number
 * contains one even and one odd digit.
*/

const oneOddOneEven = (n) => {
    const l = n / 10 | 0
    const r = n % 10

    return (l % 2 == 0 && r % 2 == 0) || (l % 2 == 1 && r % 2 == 1) ? false : true
}

console.log(oneOddOneEven(12)) // true
console.log(oneOddOneEven(55)) // false
console.log(oneOddOneEven(22)) // false

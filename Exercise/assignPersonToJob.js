/*
 * Question!
 * You have two arrays. One shows the names of the people
 * (names), while the other shows their occupation (jobs).
 * Your task is to create an object displaying each person
 * to their respective occupation.
 * Names    Jobs
 * Annie    Teacher
 * Steven   Engineer
 * Lisa     Doctor
 * Osman    Cashier
 *
 * Notes!
 * 1. The two arrays have the same length.
 * 2. The index of a name in the names array is the same
 * as the index of the person's in the (jobs) array, as
 * shown in the table.
*/

const assignPersonToJob = (names, jobs) => {
    const personToJob = {}

    names.forEach((names, i) => {
        personToJob[names] = jobs[i]
    })

    return personToJob
}

const names = ["Dennis", "Vera", "Mabel", "Annette", "Sussan"]
const jobs = ["Butcher", "Programmer", "Doctor", "Teacher", "Lecturer"]

console.log(assignPersonToJob(names, jobs))

/*
 * Result :
    {
        Dennis: "Butcher",
        Vera: "Programmer",
        Mabel: "Doctor",
        Annette: "Teacher",
        Sussan: "Lecturer"
    }
*/

/*
 * Question!
 * Create a function that returns "even" if a number has
 * an even number of factors and "odd" if a number has an
 * odd number of factors.
 *
 * Notes!
 * 1. You don't need to actually calculate the factors to
 * solve this problem.
 * 2. Think about why a number would have an odd number of
 * factors.
*/

const factorGroup = (n) => {
    return n % 2 == 0 ? 'odd' : 'even'
}

console.log(factorGroup(33)) // "even"
console.log(factorGroup(36)) // "odd"
console.log(factorGroup(7)) // "even"

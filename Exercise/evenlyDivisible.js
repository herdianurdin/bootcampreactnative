/*
 * Question!
 * Create a function that takes three arguments (a, b, c) and
 * returns the sum of the number are evenly divided by (c) 
 * from the range (a, b) inclusive.
 *
 * Notes!
 * Return (0) if there is no number between (a) and (b) that
 * can be evenly divided by (c.)
*/

const evenlyDivisible = (a, b, c) => {
    let result = 0

    for (let i = a; i <= b; i++) {
        result += i % c == 0 ? i : 0
    }

    return result
}

console.log(evenlyDivisible(1, 10, 20)) // 0
// No number between 1 and 10 can be evenly divided by 20.

console.log(evenlyDivisible(1, 10, 2)) // 30
// 2 + 4 + 6 + 8 + 10 = 30

console.log(evenlyDivisible(1, 10, 3)) // 18
// 3 + 6 + 9 = 18

/*
 * Question!
 * Create a function that takes in a number as a string (n)
 * and returns the number without trailing and leading zeros.
 * 1. Trailing Zeros are the zeros after a decimal point
 * which don't affect the value.
 * 2. Leading Zeros are the zeros before a whole number which
 * don't affect the value.
 *
 * Notes!
 * 1. Return a string.
 * 2. If you get a number with .0 on the end, return the
 * integer value.
 * 3. if the number is 0, 0.0, etc return "0"
*/

const removeLeadingTrailing = (n) => {
    n = parseFloat(n)

    return n.toString()
}

console.log(removeLeadingTrailing("230.000")) // "230"
console.log(removeLeadingTrailing("00402")) // "402"
console.log(removeLeadingTrailing("03.1400")) // "3.14"
console.log(removeLeadingTrailing("30")) // "30"

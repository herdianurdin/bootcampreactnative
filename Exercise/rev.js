/*
 * Question!
 * Create a function that takes an integer (n) and reverse it.
 *
 * Notes!
 * 1. This challenge is about using two operators that are
 * related to division.
 * 2. If th enumber is negative, treat it like it's positive.
*/

const rev = (n) => {
    n = Math.abs(n)

    let digit = 0
    let result = 0

    while (n) {
        digit = n % 10 
        result = (result * 10) + digit
        n = n / 10 | 0
    }

    return result
}

console.log(rev(5121)) // "1215"
console.log(rev(69)) // "96"
console.log(rev(-122157)) // "751221"

/*
 * Question!
 * Write a program that takes a temperature input in (celcius)
 * and converts it to Fahrenheit and Kelvin. Return the converted
 * temperature values in an array.
 *
 * Notes!
 * 1. Return calculated temperatures up to two decimal places
*/

const tempConversion = (C) => {
    const F = parseFloat((C * 9 / 5 + 32).toFixed(2))
    const K = parseFloat((C + 273.15).toFixed(2))

    return [F, K]
}

console.log(tempConversion(0)) // [32, 273.15]
// 0°C is equal to 32°F and 273.15 K.
console.log(tempConversion(100)) // [212, 373.15]
console.log(tempConversion(-10)) // [14, 263.15]
console.log(tempConversion(300.4)) // [572.72, 573.55]

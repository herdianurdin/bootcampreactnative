/*
 * Question!
 * Return the total number of arrays inside a given array.
*/

const numOfSubbarrays = (arr) => {
    return arr[0][0] ? arr.length : 0
}

console.log(numOfSubbarrays([[1, 2, 3]])) // 1
console.log(numOfSubbarrays([[1, 2, 3], [1, 2, 3], [1, 2, 3]])) // 3
console.log(numOfSubbarrays([[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]])) // 4
console.log(numOfSubbarrays([1, 2, 3])) // 0

/*
 * Question!
 * Write a function that returns an anonymous function,
 * which transforms its input by adding a particular
 * (sufflix) at the end.
*/

const add_suffix = (sufflix) => {
    const result = (str) => {
        console.log(`${str}${sufflix}`)
    }

    return result
}

add_ly = add_suffix("ly")
add_ly("hopeless") // "hopelessly"
add_ly("total") // "totally"

add_less = add_suffix("less")
add_less("fear") // "fearless"
add_less("ruth") // "ruthless"
